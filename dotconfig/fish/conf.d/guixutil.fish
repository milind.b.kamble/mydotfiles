function activate_guix_profiles
    for i in $GUIX_EXTRA_PROFILES/*
      set -x GUIX_PROFILE $i/(basename $i)
      bass source $GUIX_PROFILE/etc/profile
      set -e GUIX_PROFILE
   end
end
