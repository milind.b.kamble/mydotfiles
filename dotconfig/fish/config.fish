# the old method involved setting a global variable named __fish_login_config_sourced to determine
# if we should source $HOME/.profile. But when we switched to Wayland in gdm, I discovered that
# the PATH was getting reset. This was happeing because /etc/profile gets sourced to mimic a
# login shell. This is detailed in https://github.com/systemd/systemd/issues/6414

# so what happens is that the process tree (pstree -lspu) appears as:
#  shepherd(1)|-.gdm-real(256)-+-.gdm-session-wo(668)-+-.gdm-wayland-se(759,mbkamble)-+-dbus-daemon etc
#                                                                                      |-fish(768)---fish(884)---.gnome-session-(943)-+-
# upto fish(884) the PATH was reflecting correctly:
# __fish_login_config_sourced=1
# __extra_profiles_sourced=yes
# PATH=/gnu/store/b1m22l36946alad71l5iprq9j33llwzj-glib-2.72.3-bin/bin
# /home/mbkamble/.guix-extra-profiles/programming/programming/bin
# /home/mbkamble/.guix-extra-profiles/emacs/emacs/bin
# /home/mbkamble/.guix-extra-profiles/desktop/desktop/bin
# /home/mbkamble/.guix-extra-profiles/desktop/desktop/sbin
# /home/mbkamble/.guix-extra-profiles/cli/cli/bin
# /home/mbkamble/.guix-extra-profiles/cli/cli/sbin
# /home/mbkamble/.guix-home/profile/bin
# /home/mbkamble/.guix-home/profile/sbin
# /home/mbkamble/.config/guix/current/bin
# /run/setuid-programs
# /home/mbkamble/.guix-profile/bin
# but when gnorme-session.real is exec-ed, it launches a login shell which sources /etc/profile which unsets PATH -- and crucially
# the __fish_login_config_sourced and __extra_profiles_sourced DO NOT GET CLEARED. See /run/current-system/profile/bin/.gnome-session-real where this magic happens
# and thus 943 shows this:
# __fish_login_config_sourced=1
# __extra_profiles_sourced=yes
# PATH=/home/mbkamble/.config/guix/current/bin
# /run/setuid-programs
# /home/mbkamble/.guix-profile/bin
# /run/current-system/profile/bin
# /run/current-system/profile/sbin
# /home/mbkamble/.npm-packages/bin
# /home/mbkamble/.platformio/penv/bin
# /home/mbkamble/.local/bin
# and because __Fish_Login_Config_Sourced remains set, when fish sources $HOME/.config/fish/config.fish, we failed to source the etc/profile in guix-home and extra profiles

# so the algorithm is revamped where we unconditionally source ~/.profile in every fish invocation
# and in ~/.profile, we check the presence of guix-home/profile/bin and extraprofile/profilename/profilename/bin in PATH, and only if missing, we source the corresponding etc/profile
# fish shell invoked in 
bass source $HOME/.profile
