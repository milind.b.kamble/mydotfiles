(in-package #:nyxt-user)

#+nyxt-3
;; helper utility to read or write to any process. see $HOME/programming/commonlisp/gpg-uioplisp
(defun lispy-run-program (command &optional (input ""))
  "Run `command' with optional `input'.

`command' is a list whose first element is the name of the program to be
executed and the rest of the elements are each a command-line argument
to be passed to the program.

Returns two strings, representing what the command wrote on the standard
output and standard error respectively. The result (error code) of the
command is ignored."
  (check-type command list)
  (let ((stdout (make-string-output-stream))
        (stderr (make-string-output-stream)))
    (with-input-from-string (stdin input)
      (uiop:run-program command
                        :input stdin
                        :output stdout
                        :error-output stderr
                        :ignore-error-status t))
    (values (get-output-stream-string stdout)
            (get-output-stream-string stderr))))
;; (defparameter *keepass-mpw*
;;   (with-input-from-string
;;       (s (car (multiple-value-list
;; 	       (lispy-run-program
;; 		(list "gpg" "--no-tty" "-d"
;; 		      "/home/mbkamble/.gnupg/keepass/safebox.gpg")))))
;;     (read-line s)))

(progn
  (define-command my-copy-password (&optional (buffer (current-buffer)))
    "Query password and paste"
    (nyxt/mode/password:copy-password buffer) ;; since user has to select from shortlist, the clipboard gets populated asynchronously
    (sleep 1) ;; if we don't sleep, the paste command will paste whatever is in the clipboard at this momnent
    (nyxt/mode/document:paste buffer))
  (define-command my-copy-username (&optional (buffer (current-buffer)))
    "Query username and paste"
    (nyxt/mode/password:copy-username buffer)
    (sleep 1)
    (nyxt/mode/document:paste buffer))
  (define-command my-number-buffers ()
    "Print number of buffers present in the browser"
    ;;(format t "hello world ~a~%" 55)
    ;; (echo "hello world ~a~%" 55)
    (echo "number of buffers: ~a" (length (nyxt:buffer-list))))
  ;; the template used was based from the nyxt user manual section "Keybinding configuration"
  (defvar *my-keymap* (make-keymap "my-map"))
  (define-key *my-keymap*
    ;; "C-M-p" 'nyxt/mode/password:copy-password
    ;; "C-M-u" 'nyxt/mode/password:copy-username
    "C-M-p" 'my-copy-password
    "C-M-u" 'my-copy-username
    "C-M-n" 'my-number-buffers
    "C-x r b" 'list-bookmarks)
  (define-mode my-mode ()
    "Dummy mode for the custom key bindings in `*my-keymap*.'"
    ((visible-in-status-p nil)
     (keyscheme-map (keymaps:make-keyscheme-map
		     nyxt/keyscheme:emacs *my-keymap*))))
    
  (defvar *web-buffer-modes*
    '(my-mode
      nyxt/mode/emacs:emacs-mode
      nyxt/mode/autofill:autofill-mode
      nyxt/mode/bookmark:bookmark-mode
      nyxt/mode/hint:hint-mode
      nyxt/mode/blocker:blocker-mode
      nyxt/mode/force-https:force-https-mode
      nyxt/mode/reduce-tracking:reduce-tracking-mode
      nyxt/mode/password:password-mode
      nyxt/mode/user-script:user-script-mode)
    "The modes to enable in web-buffer by default.
Extension files (like dark-reader.lisp) are to append to this list.
Why the variable? Because one can only set `default-modes' once, so I
need to dynamically construct a list of modes and configure the slot
only after it's done.")

  (define-configuration buffer
      ((default-modes (append *web-buffer-modes* %slot-default%))))

  (defmethod initialize-instance :after
    ((interface password:keepassxc-interface) &key &allow-other-keys)
    (setf (password:password-file interface) "/home/mbkamble/.gnupg/keepass/khaazgee_milind_keepass.kdbx")
    (setf (password:key-file interface) "/home/mbkamble/.gnupg/keepass/milind_keepass.sec-key")
    (setf (password:master-password interface) ; setf master-password to a string
	  (string-trim '(#\Newline #\Space)    ; this creates the string by reading it from safebox.gpg
		       (car (multiple-value-list
			     (lispy-run-program
			      '("gpg" "--no-tty" "-d"
				"/home/mbkamble/.gnupg/keepass/safebox.gpg"))))))
    ;; (setf (password:sleep-timer) 120)
    ;; (setf (slot-value nyxt/mode/password::keepassxc-interface
    ;;                 'password:sleep-timer)
    ;;       120)
    )
  (define-configuration nyxt/mode/password:password-mode
  ((nyxt/mode/password:password-interface
    (make-instance 'password:keepassxc-interface))))
  
  ;; from nyxt documentation
  (setf nyxt/mode/certificate-exception:*default-certificate-exceptions*
      '("192.168.7.10")))
