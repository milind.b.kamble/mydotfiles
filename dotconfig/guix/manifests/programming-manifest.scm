(use-modules (programming-packages)
	     (mypackages))

(concatenate-manifests (list
			(specifications->manifest
			 (append my-python-packages
				 my-cc-packages
				 my-perl-packages
				 my-guile-packages
				 my-go-packages
				 my-rust-packages
				 my-commonlisp-packages))
			;;(packages->manifest (list my-ofxparse my-beancount))
                        ))
