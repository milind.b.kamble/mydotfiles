(use-modules (cli-packages)
	     (mypackages))
;; AFAIK, `specifications->manifest' barfs when "my-beancount" is included in `my-cli-packages'
;; workaround is to create a manifest using `packages->manifest' and `concatenate-manifests'.

(concatenate-manifests 
 (list (specifications->manifest my-cli-packages)
 (packages->manifest (list my-beancount my-ofxparse))))
