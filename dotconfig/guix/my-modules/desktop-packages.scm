(define-module (desktop-packages))

(define-public my-desktop-packages
  '(
    ;; why is man-db needed? To ensure that the <profilepath>/share/man is added to MANPATH in the <prfilepath>/etc/profile file. see https://lists.gnu.org/archive/html/help-guix/2023-09/msg00032.html
    "man-db"  ;; 
    "keepassxc"
    "wl-clipboard-x11" ;; -- needs wayland server
    "wl-clipboard"
    "copyq"    ;; CopyQ is clipboard manager with editing and scripting features.  CopyQ monitors system clipboard and saves its content in customized tabs.  Saved clipboard can be later copied and pasted directly into any application
    ;; "xclip"
    ;; "qutebrowser" not needed anymore
    "graphviz"
    "xdot"
    "xterm"  ;; fallback when gnome-terminal fails
    "ungoogled-chromium"
    "google-chrome-unstable"            ; Google chrome
    "vlc"
    "okular"
    "authenticator"  ;; two-factor authentication (2FA) application built for the GNOME desktop
    "rofi"
    "pinentry"
    "pinentry-rofi"
    "pinentry-gnome3"
    "pinentry-tty"
    "imagemagick:out"
    "blender"
    "inkscape"
    "gimp:out"
    "nsxiv"
    "mps-youtube"
    "mpv"
    ;; "youtube-viewer" ;; seems to depend on perl-gtk2 which fails in some test case
    ;; "nyxt"        ;; common-lisp based browser. commenting because we build and install from source
    ;; gst-plugins* are for playing embedded videos in nyxt browser
    "gst-plugins-base"
    "gst-plugins-good"
    "gst-plugins-bad"
    "gst-plugins-ugly"
    ;; libreoffice for odt files
    "libreoffice"
    ))
   

