(define-module (mypackages)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (gnu packages finance)         ;; for beancount
  #:use-module (gnu packages python-xyz)      ;; for ofxparse
  #:use-module (gnu packages python-science)  ;; for pandas
  #:use-module (gnu packages xml)             ;; for lxml
  #:use-module (guix gexp)                    ;; needed for beancont spec where we use gexp in snippet
  #:use-module (guix build-system python)     ;; for pypi-uri
  #:use-module (guix git-download)            ;; for git-version, git-fetch etc.


  ;; trying to hack to overcome a fatal error "search-patches: unbound variable"
  #:use-module (gnu packages) ;; needed for binding of var "search-patches"
  )


(define-public my-beancount-does-not-work
  (let ((commit "75a79c3ce507407d782b211e1c51e22ea7bb1c93")
	(revision "1"))
    (package
      (inherit beancount)
      (version (git-version (package-version beancount) revision commit))
      (source
       (origin
	 (method git-fetch)
	 (uri (git-reference
	       (url "file:///home/mbkamble/opensource/beancount") ; this does not work because guix-daemon runs in chroot env where /home is not available
	       (commit commit)))
	 (file-name (git-file-name (package-name beancount) version))
	 (sha256 (base32 "11xjywwqwvnk74pifb4xas2rchcihgcv8nn1gb9wvcd6g4avww9p"))
	 )))))

;; similarly, we need the latest version of lxml because it is more robust than the official guix version
;; defined in gnu/packages/xml.scm
(define-public my-lxml			      ;
  (package
   (inherit python-lxml)
   (version "4.9.2")
   (source
    (origin
     ;; (inherit (package-source python-lxml))
     (method url-fetch)
     (uri (pypi-uri "lxml" version))
     (sha256 (base32 "0rsvhd03cv7fczd04xqf1idlnkvjy0hixx2p6a5k6w5cnypcym94"))))))

;; update bsoup to use the ew lxml
(define-public my-beautifulsoup
  (package
   (inherit python-beautifulsoup4)
   (version "4.11.2")
   (source
    (origin
     (method url-fetch)
     (uri (pypi-uri "beautifulsoup4" version))
     (sha256 (base32 "01h1k8s91khl2cm53zdxavc0vjaxyir8vyrnfjca5rbxf6kdsjxw"))))
   (propagated-inputs (modify-inputs
		       (package-propagated-inputs python-beautifulsoup4)
                       (delete "python-lxml")
                       (append my-lxml)))))

;; use my enhancements for beancount created over latest head of beancount-v2
;; this defines `my-beancount' as a new package alongside the original beancount
;; the original beancount is defined in ~/.config/guix/current/share/guile/site/3.0/gnu/packages/finance.scm
(define-public my-beancount
  (let ((commit "f5250d7975a8b8911b6d24fef31e76b6a6514b6a")
	(revision "1"))
    (package
      (inherit beancount)
      ;;(version (git-version (package-version beancount) revision commit))
      (source
       (origin
	 (method url-fetch)
	 (uri "https://gitlab.com/milind.b.kamble/beancount/-/archive/v2/beancount-v2.tar.gz")
	 (sha256 (base32 "0s8mjils84g4m3qrw25gz25xdi9zq47jr0p6y8c0yp31p6a0jdy4"))
         (patches (search-patches "beancount-disable-googleapis-fonts.patch"))
         (modules '((guix build utils)))
         (snippet
          #~(begin
              ;; Remove broken experiments.
              (delete-file-recursively "experiments")
              ;; Remove bundled packages.
              (delete-file-recursively "third_party")))
         ))
      (propagated-inputs (modify-inputs
			  (package-propagated-inputs beancount)
			  (delete "python-beautifulsoup4" "python-lxml")
                          (append my-beautifulsoup my-lxml python-pandas)))
      (arguments
       (list
        #:tests? #f  ;; mbkamble hack
        ;; test-flags and phases are from beancount package spec in (gun packages finance)
        #:test-flags
        #~(list "-k" (string-append
                      ;; ModuleNotFoundError: No module named 'pytest'
                      "not test_parse_stdin"
                      ;; AssertionError: 5 not greater than 20
                      " and not test_setup"))
        #:phases
        #~(modify-phases %standard-phases
            (add-after 'unpack 'relax-requirements
              (lambda _
                (substitute* "setup.py"
                  ;; Use compatible fork, and do not fail during sanity check.
                  (("\"pdfminer2\",") ""))))
            (add-before 'check 'build-extensions
              (lambda _
                (invoke "python" "setup.py" "build_ext" "--inplace"))))))
      )))


;; ofxparse also needs to be tweaked to use both the updated depedencies as well as my changes on top of upstream
(define-public my-ofxparse
  (package
   (inherit python-ofxparse)
   (version "0.21.1")
   (source
    (origin
     (method url-fetch)
     (uri "https://gitlab.com/milind.b.kamble/ofxparse/-/archive/master/ofxparse-master.tar.gz")
     (sha256 (base32 "1dqk8a2fxvcpg50zysav063637h9360n1aqdh9l83aw0890jmrcp"))))
    (propagated-inputs (modify-inputs
			(package-propagated-inputs python-ofxparse)
			(delete "python-beautifulsoup4" "python-lxml")
                        (append my-beautifulsoup my-lxml python-pandas)))
    (arguments
     '(#:tests? #t
       #:phases
       (modify-phases %standard-phases
         (replace 'check
           (lambda* (#:key tests? #:allow-other-keys)
             (when tests?
               (invoke "nosetests" "-v")))))))
    ))

;; to install using these new package names you need to run like this
;; for profile in programming  ; guix package -K   -p $HOME/.guix-extra-profiles/$profile/$profile \
;; -L $HOME/opensource/mydotfiles/dotconfig/guix/my-modules/  -f /tmp/junk.scm  ; end
;; where junk.scm contains
;; (use-modules (mypackages))
;; my-ofxparse


;; when source is specified or overridden for a package,
;; to obtain sha256 base32 encoding, run shell command
;; guix download -H sha256 -f nix-base32 <url to tar>
;; the last line from the output is the sha256 in base32
;; (source
;;     (origin
;;      (method url-fetch)
;;      (uri "https://gitlab.com/milind.b.kamble/ofxparse/-/archive/master/ofxparse-master.tar.gz")
;;      (sha256 (base32 "1dqk8a2fxvcpg50zysav063637h9360n1aqdh9l83aw0890jmrcp"))))
