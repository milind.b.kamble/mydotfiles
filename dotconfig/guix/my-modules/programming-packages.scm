(define-module (programming-packages)
  #:use-module (mypackages))

(define-public my-python-packages
  '(
    "man-db" ; so that MANPATH update is included in <profilepath>/etc/profile
    "python"
    "python-pandas"
    "python-ipython"
    "python-ipdb"
    "python-pyserial"
    "python-fuzzywuzzy"
    "python-progress"
    "python-tabulate"
    "python-pylint"
    "python-black"
    "python-flake8"
    "python-setuptools"
    "python-pdfminer-six"
    "python-pypdf2"
    "python-pdftotext"
    "python-esptool"
    "python-fido2"                      ; attempting to use Solokey USB security key
    ))

(define-public my-cc-packages
  '(
    "cmake"))

(define-public my-perl-packages
  '("perl"))

(define-public my-guile-packages
  '(
    "guile"
    ))

(define-public my-go-packages
  '(
    "go"))

(define-public my-rust-packages
  '(
    ;; "rust"
    ;; "rust-cargo"
    ))

(define-public my-commonlisp-packages
  '(
    "sbcl"
    ))
