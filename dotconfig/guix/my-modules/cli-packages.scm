(define-module (cli-packages))

(define-public my-cli-packages
  '(
    "acpi"             ; one usage is to get batt info using "acpi -b"
    "adb"
    ;; "arm-none-eabi-toolchain"
    ;; "avr-toolchain"
    "binutils"
    "curl"
    "dbus-verbose"
    "dfu-programmer"
    "dfu-util"
    "dmidecode"
    "dos2unix"
    "efibootmgr"
    "enchant"
    "eza"                               ; replacement for ls
    "fastboot"
    "fd"                    ; replacement for find (~80% of use cases)
    "ffmpeg"
    "file"
    "git" "mercurial"   
    ;; "glib"           ; for access to command line tools like gio, gdbus, gesttings in eshell
    "gptfdisk"                          ; aka gdisk
    "grep"
    "info-reader"                       ; needed for info pages
    "ispell"
    ;; libdmtx is software for reading and writing Data Matrix 2D barcodes of the modern ECC200 variety
    "libdmtx" 
    "lshw"
    "lsof" "lsofgraph"
    "make"
    ;; man-db needed for man pages (see https://guix.gnu.org/cookbook/en/guix-cookbook.html
    "man-db" 
    "ncurses"
    "ncurses"                           ; needed for tput
    "ntfs-3g"
    "pandoc"
    ;; Paperkey extracts the secret bytes from an OpenPGP (GnuPG, PGP, etc) key for printing with paper and ink
    "paperkey" 
    "perl-image-exiftool"
    "picocom"
    "pkg-config"
    "poppler"      ; for pdftotext
    "powerstat"    ; reports computer's power consumption in real time
    "pup"          ; for processing HTML inspired by jq
    ;; The capacity of QR Code is up to 7000 digits or 4000 characters, and is highly robust
    "qrencode"
    "ripgrep"      ; for command line and emacs counsel-rip-grep
    "rlwrap"
    "rsync"
    "sbcl-enchant"
    "strace"
    "tmux"
    "tree-sitter-org"                   ; grammar for Org
    "tree-sitter-python"                ; grammar for Python
    "tree-sitter-cpp"
    "tree-sitter-c" 
    "tree-sitter-cmake"

    ;; exfat stuff for using builtin cardreader of laptop
    "exfatprogs"
    "exfat-utils"
    "fuse-exfat"
    
    ;; udiskie is for unmounting removable media. See also udisksctl
    "udiskie"
    "unzip"
    ;; ; https://unix.stackexchange.com/questions/63769/fast-tool-to-generate-thumbnail-video-galleries-for-command-line
    "video-contact-sheet"
    "which"
    "xdg-utils"           ; provides xdg-open,settings,email,mime etc.
    "xxd"
    "zbar"                       ; versatile barcode and qrcode reader
    "zip"
    ))
