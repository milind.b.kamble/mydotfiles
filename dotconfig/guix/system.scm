(use-modules (gnu) (nongnu packages linux)
	     (gnu packages bash)
             (gnu packages compression)
	     (gnu packages embedded)
	     (gnu packages android) ; for android-udev-rules
	     (gnu system shadow)    ; for user-group
	     (gnu packages gcc))
(use-service-modules
 cups
 desktop
 networking
 ssh
 xorg)
(use-package-modules shells) ;; tmux gnupg python fonts graphviz wget binutils image-viewers)

(define %xorg-libinput-config
  "Section \"InputClass\"
  Identifier \"Touchpads\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsTouchpad \"on\"

  Option \"Tapping\" \"on\"
  Option \"TappingDrag\" \"on\"
  Option \"DisableWhileTyping\" \"on\"
  Option \"MiddleEmulation\" \"on\"
  Option \"ScrollMethod\" \"twofinger\"
EndSection
Section \"InputClass\"
  Identifier \"Keyboards\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsKeyboard \"on\"
EndSection
")

;; based on https://unix.stackexchange.com/questions/701630/udev-rule-applies-but-doesnt-set-the-group
(define stm32duinobl-udev-rule
  (udev-rule "49-stm32duinobl.rules"
             "#bootloader-mode\nSUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1eaf\", ATTRS{idProduct}==\"0003\", GROUP=\"lp\"\nSUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1eaf\", ATTRS{idProduct}==\"0004\", GROUP=\"lp\"\n"))

;; following is from https://gitlab.com/nonguix/nonguix (substitutes for nonguix)
(define nonguix-dektop-services
  (modify-services %desktop-services
             (guix-service-type config => (guix-configuration
               (inherit config)
               (substitute-urls
                (append (list "https://substitutes.nonguix.org")
                  %default-substitute-urls))
               (authorized-keys
                (append (list
			 (plain-file "nonguix.pub"
				     "\
(public-key 
 (ecc 
  (curve Ed25519)
  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
  )
 )"))
			%default-authorized-guix-keys))))))
   
(operating-system
 (kernel linux)
 (firmware (list linux-firmware))
 (locale "en_US.utf8")
 (timezone "America/Chicago")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "kamble9343guix")
 
 (users (cons* (user-account
		(name "mbkamble")
		(comment "Milind Kamble")
		(group "users")
		(home-directory "/home/mbkamble")
		(shell (file-append fish "/bin/fish"))
		(supplementary-groups
		 '("wheel" "netdev" "audio" "video" "lp" "dialout" "adbusers")))
	       (user-account
		(name "tester1")
		(comment "Tester Kamble")
		(group "users")
		(home-directory "/home/tester1")
		;; (shell (file-append fish "/bin/fish"))
		(supplementary-groups
		 '("wheel" "netdev" "audio" "video" "lp")))
	       %base-user-accounts))
 
 (packages
  (append
   (list
    ;; system tools
    ;; (specification->package "nss-certs")
    (specification->package "glibc")
    (specification->package "glibc-locales")
    (specification->package "patchelf")
    
    ;; mashup from daviwil/Sytems.org and msheikh/minimal-os
    (specification->package "bluez")
    ;; Xorg
    (specification->package "libinput")
    ;; needed for home-environment bringup
    (specification->package "fish")
    (specification->package "gptfdisk")
    (specification->package "bash")
    (specification->package "python")
    (specification->package "stlink")
    (specification->package "fuse")   ;; needed for AppImage execution and (probably) many other apps
    )
   %base-packages))
 
 (services
  (append         ;; appends the 2nd arg to 1st arg. Both args have to be lists
   (list (service gnome-desktop-service-type)
	 (service openssh-service-type)
	 (service cups-service-type)
	 (bluetooth-service-type #:auto-enable? #t)
	 (extra-special-file "/lib64/ld-linux-x86-64.so.2" ;; based on https://www.reddit.com/r/GUIX/comments/hxi5fj/comment/fz7bkow/
			     (file-append glibc "/lib/ld-linux-x86-64.so.2"))
	 (extra-special-file "/bin/bash"  ;; platformio scripts have hardcoded /bin/bash 
			     (file-append bash "/bin/bash"))
	 (extra-special-file "/lib64/libz.so.1"   ;; hacking for executing AppImage
			     (file-append zlib "/lib/libz.so.1"))
	 ;; (extra-special-file "/lib64/libstdc++.so.6"   ;; needed for riscv32-esp-elf-g++ (seeed_xiao_esp32c3 board)
	 ;; 		     (file-append  `(,gcc "lib") "/lib/libstdc++.so.6"))
	 (set-xorg-configuration
	  (xorg-configuration
	   (keyboard-layout keyboard-layout)
	   (extra-config (list %xorg-libinput-config))))
	 ;; cant send multiple file->udev-rule files as parameters even though udev-rules-service definition indicates
	 ;; udev-rules-service [NAME RULES], ie RULES is plural. In the body, it wraps the RULES argument in a list
	 (udev-rules-service 'stlink
			     (file->udev-rule "49-mbkstlinkv2.rules"
					      #~(string-append #$stlink "/etc/udev/rules.d/49-stlinkv2.rules")))
	 (udev-rules-service 'qmk
			     (file->udev-rule "50-qmk.rules"
					      (local-file "50-qmk.rules")))
	 ;; here android-udev-rules is a package wherein the rules files are present in its lib/udev/rules.d directory
	 ;; for stlink package, the rules files are located in etc/udev/rules.d and therefore it cannot be specified as a second argument in the above call
	 (udev-rules-service 'adb android-udev-rules #:groups '("adbusers")))
   nonguix-dektop-services))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))

 (swap-devices
  (list (swap-space
	 (target
	  (uuid "db35b3c6-8f65-41a9-91af-e8c0bdb28fad")))))

 (mapped-devices
  (list (mapped-device
	 (source
	  (uuid "52820bd7-9b0a-419c-a744-357943b252c2"))
	 (target "home")
	 (type luks-device-mapping))
	(mapped-device
	 (source
	  (uuid "6ec1485e-0424-406e-848f-9f2334159da8"))
	 (target "guixroot")
	 (type luks-device-mapping))))

 (file-systems
  (cons* (file-system
	  (mount-point "/home")
	  (device "/dev/mapper/home")
	  (type "ext4")
	  (dependencies mapped-devices))
	 (file-system
	  (mount-point "/")
	  (device "/dev/mapper/guixroot")
	  (type "ext4")
	  (dependencies mapped-devices))
	 (file-system
	  (mount-point "/boot/efi")
	  (device (uuid "24D8-3A48" 'fat32))
	  (type "vfat"))
	 %base-file-systems))

 )
