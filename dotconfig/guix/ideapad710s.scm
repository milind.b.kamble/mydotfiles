;; see https://nonguix.org/, click on "nonguix" and go through "next" pages to see which build shows non-zero success number. I pin the version of guix and nongnu to it
;; see https://github.com/SystemCrafters/guix-installer/blob/master/.github/workflows/build.yaml and TBD: locate name: PromyLOPh/guix-install-action@v1 to understand how to build linux pacakge in github workflow

(use-modules (gnu) (nongnu packages linux) (nongnu system linux-initrd)
             (guix inferior) (guix channels) (srfi srfi-1)
             (gnu packages linux)
	     (gnu packages bash)
             (gnu packages compression)
	     (gnu packages embedded)
	     (gnu packages android) ; for android-udev-rules
	     (gnu system shadow)    ; for user-group
             (gnu packages security-token) ; for libfido2
	     (gnu packages gcc))
(use-service-modules
 cups
 desktop
 networking
 ssh
 xorg)
(use-package-modules shells)

(define %xorg-libinput-config
  "Section \"InputClass\"
  Identifier \"Touchpads\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsTouchpad \"on\"

  Option \"Tapping\" \"on\"
  Option \"TappingDrag\" \"on\"
  Option \"DisableWhileTyping\" \"on\"
  Option \"MiddleEmulation\" \"on\"
  Option \"ScrollMethod\" \"twofinger\"
EndSection
Section \"InputClass\"
  Identifier \"Keyboards\"
  Driver \"libinput\"
  MatchDevicePath \"/dev/input/event*\"
  MatchIsKeyboard \"on\"
EndSection
")

;; based on https://unix.stackexchange.com/questions/701630/udev-rule-applies-but-doesnt-set-the-group
(define stm32duinobl-udev-rule
  (udev-rule "49-stm32duinobl.rules"
             "#bootloader-mode\nSUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1eaf\", ATTRS{idProduct}==\"0003\", GROUP=\"lp\"\nSUBSYSTEMS==\"usb\", ATTRS{idVendor}==\"1eaf\", ATTRS{idProduct}==\"0004\", GROUP=\"lp\"\n"))

;; following is from https://gitlab.com/nonguix/nonguix (substitutes for nonguix)
(define nongnu-desktop-services
  (modify-services %desktop-services
		   (guix-service-type config => (guix-configuration
						 (inherit config)
						 (substitute-urls
						  (append (list "https://substitutes.nonguix.org")
							  %default-substitute-urls))
						 (authorized-keys
						  (append (list
							   (plain-file "nonguix.pub"
								       "\
(public-key 
 (ecc 
  (curve Ed25519)
  (q #C1FD53E5D4CE971933EC50C9F307AE2171A2D3B52C804642A7A35F84F3A4EA98#)
  )
 )"))
							  %default-authorized-guix-keys))))
		   (gdm-service-type config => (gdm-configuration
						(inherit config)
						(wayland? #t) ;; quick expt to see if keepassxc-cli can be used with wayland (https://github.com/keepassxreboot/keepassxc/issues/4498). use wl-copy --clear to clear the clipboard
						;; (wayland? #f);; turning off wayland (default) because keepassxc-cli clip does not work with wayland
						)))) 

(define my-kernel
  (let*
      ((channels
        (list (channel
               (name 'nonguix)
               (url "https://gitlab.com/nonguix/nonguix")
               (commit "2e166ea5d25f2d8dbcf10c87b7d4aa74f3274754")
               (introduction
                (make-channel-introduction
                 "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
                 (openpgp-fingerprint
                  "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
              (channel
               (name 'guix)
               (url "https://git.savannah.gnu.org/git/guix.git")
               (commit "7e0ad0dd0f2829d6f3776648ba7c88acf9888d7a"))))
       (inferior
        (inferior-for-channels channels)))
    (first (lookup-inferior-packages inferior "linux" "6.0.9"))))

(operating-system
 ;; (kernel my-kernel)
 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))
 ;; (firmware (cons* iwlwifi-firmware ibt-hw-firmware i915-firmware
 ;;                 %base-firmware))
 (locale "en_US.utf8")
 (timezone "America/Chicago")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "ideapad710s")
 
 (users (cons* (user-account
		(name "mbkamble")
		(comment "Milind Kamble")
		(group "users")
		(home-directory "/home/mbkamble")
		(shell (file-append fish "/bin/fish"))
		(supplementary-groups
		 '("wheel" "netdev" "audio" "video" "lp" "dialout" "adbusers")))
	       (user-account
		(name "tester1")
		(comment "Tester Kamble")
		(group "users")
		(home-directory "/home/tester1")
		;; (shell (file-append fish "/bin/fish"))
		(supplementary-groups
		 '("wheel" "netdev" "audio" "video" "lp")))
	       %base-user-accounts))
 
 (packages
  (append
   (list
    ;; system tools
    ;; (specification->package "nss-certs")
    (specification->package "glibc")
    (specification->package "glibc-locales")
    (specification->package "patchelf")
    (specification->package "linux-libre-headers")
    (specification->package "git")
    (specification->package "curl")
    (specification->package "texinfo")  ;; required for 'install-info' which is reqired for el-get
    (specification->package "jq")
    (specification->package "wireguard-tools")
    (specification->package "fish")
    (specification->package "gnupg")
    ;; libfido2 supercedes libu2f-host. provides library functionality and command-line tools to communicate with a FIDO device over USB or NFC, and to verify attestation and assertion signatures
    (specification->package "libfido2")
    (specification->package "pinentry-tty")
    (specification->package "pinentry-rofi")
    
    ;; mashup from daviwil/Sytems.org and msheikh/minimal-os
    (specification->package "bluez")
    ;; Xorg
    (specification->package "libinput")
    ;; needed for home-environment bringup
    (specification->package "fish")
    (specification->package "gptfdisk")
    (specification->package "ntfs-3g")
    (specification->package "exfatprogs")
    (specification->package "bash")
    (specification->package "python")
    (specification->package "stlink")
    (specification->package "fuse")   ;; needed for AppImage execution and (probably) many other apps
    (specification->package "texlive-bin")
    (specification->package "texinfo")
    )
   %base-packages))
 
 (services
  (append         ;; appends the 2nd arg to 1st arg. Both args have to be lists
   (list (service gnome-desktop-service-type)
	 (service openssh-service-type)
	 (service tor-service-type)
	 (service cups-service-type)
	 (service bluetooth-service-type (bluetooth-configuration (auto-enable? #t)))
	 (extra-special-file "/lib64/ld-linux-x86-64.so.2" ;; based on https://www.reddit.com/r/GUIX/comments/hxi5fj/comment/fz7bkow/
			     (file-append glibc "/lib/ld-linux-x86-64.so.2"))
	 (extra-special-file "/bin/bash"  ;; platformio scripts have hardcoded /bin/bash 
			     (file-append bash "/bin/bash"))
	 (extra-special-file "/lib64/libz.so.1"   ;; hacking for executing AppImage
			     (file-append zlib "/lib/libz.so.1"))
	 ;; (extra-special-file "/lib64/libstdc++.so.6"   ;; needed for riscv32-esp-elf-g++ (seeed_xiao_esp32c3 board)
	 ;; 		     (file-append  `(,gcc "lib") "/lib/libstdc++.so.6"))
	 (set-xorg-configuration
	  (xorg-configuration
	   (keyboard-layout keyboard-layout)
	   (extra-config (list %xorg-libinput-config))))
	 ;; cant send multiple file->udev-rule files as parameters even though udev-rules-service definition indicates
	 ;; udev-rules-service [NAME RULES], ie RULES is plural. In the body, it wraps the RULES argument in a list
	 (udev-rules-service 'stlink
			     (file->udev-rule "49-mbkstlinkv2.rules"
					      #~(string-append #$stlink "/etc/udev/rules.d/49-stlinkv2.rules")))
	 (udev-rules-service 'qmk
			     (file->udev-rule "50-qmk.rules"
					      (local-file "50-qmk.rules")))
	 ;; here android-udev-rules is a package wherein the rules files are present in its lib/udev/rules.d directory
	 ;; for stlink package, the rules files are located in etc/udev/rules.d and therefore it cannot be specified as a second argument in the above call
	 (udev-rules-service 'adb android-udev-rules #:groups '("adbusers")))
   nongnu-desktop-services))
 ;; (kernel-loadable-modules (list wireguard))

 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))

 (swap-devices
  (list (swap-space
	 (target
	  (uuid "2526c45c-3184-4d99-8c3b-c9e25cba41b9")))))

 (file-systems (cons* (file-system
                       (mount-point "/home")
                       (device (uuid
                                "632dc13a-9e33-4450-9399-849f9cfbdbf4"
                                'ext4))
                       (type "ext4"))
                      (file-system
                       (mount-point "/")
                       (device (uuid
                                "ec6f0ca7-ffa6-4dff-81a0-bd200f46ee54"
                                'ext4))
                       (type "ext4"))
                      (file-system
                       (mount-point "/boot/efi")
                       (device (uuid "1D79-F667"
                                     'fat32))
                       (type "vfat")) %base-file-systems))
 )
