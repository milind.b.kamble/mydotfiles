(defvar mbk/default-gc-cons-threshold gc-cons-threshold
  "Backup of the default GC threshold.")
(defvar mbk/default-gc-cons-percentage gc-cons-percentage
  "Backup of the default GC cons percentage.")
;; and reset it to "normal" when done
(add-hook 'emacs-startup-hook
          (lambda ()
            (setq gc-cons-threshold mbk/default-gc-cons-threshold
                  gc-cons-percentage mbk/default-gc-cons-percentage)))

(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.6
      package-enable-at-startup nil     ;; Packages will be initialized by use-package later
      ;; package-archives '(("gnu-devel" . "https://elpa.gnu.org/devel/")
      ;; 			 ("nongnu-devel" . "https://elpa.gnu.org/nongnu-devel/")
      ;; 			 ("melpa-devel" . "https://melpa.org/packages/"))
      ;; package-user-dir "~/.emacs.d/emacs-packages" ;; we don't intend to use this, but just controlling where the bultin package system will install if we issue an install command by mistake
      ;; package-archive-priorities '(("gnu-devel" . 2)
      ;; 				   ("nongnu-devel" . 1)
      ;; 				   ("melpa-devel" . 0))
      ;; package-archive-column-width 14
      ;; package-version-column-width 26
      frame-inhibit-implied-resize t    ;; Do not resize the frame at this early stage
      ;; comp-deferred-compilation nil     ;; Prevent unwanted runtime builds. Packages are compiled at install and site files are compiled when gccemacs is installed.
      )

;; Ignore Xresources
(advice-add #'x-apply-session-resources :override #'ignore)
;; TODO: Probably the better approach is:
;; (setq inhibit-x-resources t)


;; From doom-emacs
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)


;; Early no-littering
(when (and (fboundp 'startup-redirect-eln-cache)
           (boundp 'native-comp-eln-load-path))
  (startup-redirect-eln-cache
   (convert-standard-filename
    (expand-file-name  "var/eln-cache/" user-emacs-directory))))

;; user-emacs-directory/lisp contains my elisp snippets
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
