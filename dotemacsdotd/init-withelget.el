;;; Notes
;; initial bindings in emacs before we customize anything
;; `negative-argument' : global-map : C--, M-- and C-M--  -- we will rebind M-- and C-M--
;; M-_: undefined
;; `undo' : global-map : C-_ and C-/ and C-x u
;; `undo-redo' : global-map : C-? and C-M-_
;; C-x-r-u, C-x-r-U : undefined
;; when undo-tree mode is enabled
;; `undo-tree-undo' : undo-tree-map : C-_ and C-/
;; `undo-tree-visualize' : undo-tree-map : C-x u
;; `undo-tree-redo' : undo-tree-map : M-_ and C-?
;; `undo-tree-*save/restore-from/to-register' : undo-tree-map : C-x-r-{u,U}

;; For elisp code (ie when buffer major mode is emacs-lisp-mode) the regexp for header is
;; 3 or more semicolon or beginning of lisp exp or autoload

;;; Initilize or customize builtins
;; In elisp there is a ignore function, but that evaluates its argument, yielding always nil, whereas comment doesn't evaluate its body. It's useful to temporary disable bits of code.
;; source: https://www.omarpolo.com/dots/emacs.html
(defmacro comment (&rest _body)
  "Ignore BODY, just like `ignore', but this is a macro."
  '())

;; * customize some builtin variables
(progn
  (fset 'yes-or-no-p 'y-or-n-p)
  (setopt  ;; a macro similar to setq but meant for user options that uses customize machinery to set variables
   mbk/full-name "Milind Kamble"
   mbk/email "milindbkamble@gmail.com"
   
   auto-save-visited-interval 5
   require-final-newline t

   ;; view-mode has special key bindings that can be viewed by pressing `h'
   view-read-only t ;; making the buffer read-only with ‘C-x C-q’ also enables View mode

   ;; outline-minor-mode
   outline-minor-mode-cycle t
   outline-minor-mode-cycle-filter 'eolp ;; choices are eolp, bolp, not-bolp or custom defined func
   
   ;; ElDoc string in inor-mode-alist is controlled by eldoc-minor-mode-string.
   eldoc-minor-mode-string nil ;; default " ElDoc"
   
   ;; ignore case for completions
   completion-ignore-case t  ;; consider case significant in completion.
   read-file-name-completion-ignore-case t ;; for file-name completion.
   read-buffer-completion-ignore-case t ;; for buffer name completion.
   
   indent-tabs-mode nil ;; use spaces instead of tabs
   isearch-lazy-count t ;; Some very small tweaks for isearch
   search-whitespace-regexp ".*?" ;; words seperated by space in isearch input get matched regardless of the intervening chars
   isearch-allow-scroll 'unlimited
   password-cache-expiry 86400 ;; 24hrs before sudo password is prompted again

   ;; `repeat-mode-exit-key' is the key to exit repeat mode. <RET> is recommended in manual and sensible
   repeat-exit-key "RET"
   
   ;; *** disable native VC backends since we will use magit 
   ;; Feature `vc-hooks' provides hooks for the Emacs VC package. We
   ;; don't use VC, because Magit is superior in pretty much every way.
   vc-handled-backends nil
   
   desktop-restore-frames nil ;; uses frame parameters (fonts, faces etc) from init file instead from previous session
   epa-armor t
   epa-file-name-regexp "\\.\\(gpg\\|asc\\)$"
   epg-pinentry-mode 'ask	   ; use minibuffer reading passphrase
   epa-file-encrypt-to (list mbk/email)

   ;; dabbrev is buitlin with emacs
   dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'") ;; \(?:regex\) This is called shy group. Group for precedence, but no capture.
   )

  ;; ux elements
  (set-face-attribute 'default nil :family "Victor Mono" :height 120)
  ;; As per https://protesilaos.com/codelog/2020-09-05-emacs-note-mixed-font-heights/
  ;; we need to specify height of other fonts relative to default for text-scaling to work
  ;; implicit height does not work, we need to explicitly specify the :height 1.0
  (set-face-attribute 'fixed-pitch nil :family "Victor Mono" :height 1.0)
  (set-face-attribute 'variable-pitch nil :family "Source Sans 3" :height 1.0) ;; for proportional or dynamic font width
  (variable-pitch-mode -1) ;; disable varibale-pitch by default
  (defalias 'proportional-font-mode 'variable-pitch-mode
    "porportional-font is easier to remember than variable-pitch") ;; my own alias since I tend to forget `variable-pitch-mode'

  (defalias 'toggle-wrap-lines 'toggle-truncate-lines
    "wrap-lines is easier to remember than truncate-lines")
  (defalias 'dired-edit-in-place 'dired-toggle-read-only "rename files by editing dired like a regular buffer")

  (defun mbk/customize-poet (theme)
    "Customize poet theme"
    (when (eq theme 'poet)
      (custom-theme-set-faces 'user  ;; need to use `user'. `poet is overridden by `user''
			      '(font-lock-comment-face
                                ;; weight = thin|normal|heavy
			        ((t (:slant italic :weight normal)))))))
  (add-hook 'enable-theme-functions #'mbk/customize-poet)

    ;; modeline customization
  ;;**** customize modeline-modified indicator using chain icon 
  ;; mode-line-modified is a mode line construct for displaying whether current buffer is modified.
  ;; we beautify it with faicon (fa means fontawesome family)
  ;; see also https://github.com/domtronn/all-the-icons.el/wiki/Mode-Line
  (defun mbk/modeline-modified ()
    (let* ((config-alist   ;; hash like datastructure. key . (family icon-select-func parameter-for-icon-select)
	    ;; format-mode-line returns a string representing modified status of current buffer
	    ;; "*" means modifed, "-" means unmodified, "%" means read-on;y
            '(("*" all-the-icons-faicon-family all-the-icons-faicon
               "chain-broken" :height 1.2 :v-adjust -0.0)
              ("-" all-the-icons-faicon-family all-the-icons-faicon
               "link" :height 1.2 :v-adjust -0.0)
              ("%" all-the-icons-octicon-family all-the-icons-octicon
               "lock" :height 1.2 :v-adjust 0.1)))
           (result (cdr (assoc (format-mode-line "%*") config-alist))))

      (propertize (format "%s" (apply (cadr result) (cddr result)))
                  'face `(:family ,(funcall (car result)) :inherit ))))
  ;;wait (setq-default mode-line-modified '(:eval (mbk/modeline-modified)))
  ;;**** indiccate major-mode by suitable icon
  ;; mode-line-client-mode is ':' for normal frames, @ for emacsclient frames
  ;;**** major mode indicator is constructed by 'mode-name'
  ;; we can change the font to italic by customizing mode-line-buffer-id
  (defun mbk/modeline-mode-name ()
    (let* ((icon (all-the-icons-icon-for-mode major-mode))
	   (face-prop (and (stringp icon) (get-text-property 0 'face icon))))
      (when (and (stringp icon) (not (string= major-mode icon)) face-prop)
	(setq mode-name (propertize icon 'display '(:ascent center))))))
  ;;wait (add-hook 'after-change-major-mode-hook #'mbk/modeline-mode-name)

  ;; repeat-mode is a gem
  (add-hook 'find-file-hook #'repeat-mode) ;; enabling repeat mode for all files visited

  (add-hook 'text-mode-hook #'outline-minor-mode)
  (add-hook 'prog-mode-hook #'outline-minor-mode)
  ;; instead of ellipsis (...) we display a glyph when lines are hidden. see info::Display Tables
  ;; from https://emacs.stackexchange.com/questions/10981/changing-the-appearance-of-org-mode-hidden-contents-ellipsis
  (set-display-table-slot standard-display-table 
                        'selective-display (string-to-vector "⤵"))
  )


(require 'bind-key)

;;; setup el-get
(add-to-list 'load-path (expand-file-name "el-get/el-get" user-emacs-directory))
(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(unless (require 'el-get nil t)
  (with-current-buffer
      (url-retrieve-synchronously
       "https://raw.github.com/dimitri/el-get/master/el-get-install.el")
    (let (el-get-master-branch el-get-install-shallow-clone)
    (goto-char (point-max))
    (eval-print-last-sexp)
    (el-get-elpa-build-local-recipes)
    )))


(add-to-list 'el-get-recipe-path (expand-file-name "el-get-user/recipes" user-emacs-directory))
(setq
 el-get-user-package-directory (expand-file-name "el-get-init-files/" user-emacs-directory)
 el-get-git-shallow-clone t
 el-get-verbose t)

;; (setq el-get-sources
;;       '((:name magit
;;                :after (global-set-key (kbd "C-x g") 'magit-status))
;; 	(:name consult)
;; 	(:name yasnippet)))
(setq my-packages
      '(el-get))
;;(el-get-cleanup my-packages)
(el-get 'sync my-packages)


;;; Enable packages
;;;; `no-littering'
(el-get-bundle! no-littering
  :features (recentf)
  (progn
    (setq
     auto-save-file-name-transforms     ;; transform names of saved files
     `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))
     backup-directory-alist ; store all backup and autosave files in "backups"
     `((".*" . ,(no-littering-expand-var-file-name "backups/")))
     custom-file (no-littering-expand-etc-file-name "custom.el")
     recentf-max-saved-items 3001) ; no-littering sets the recent-save-file
    (recentf-mode 1)
    (auto-save-visited-mode 1) ;; automatic saving of file-visiting buffers to their files
    (savehist-mode 1) ;; enable saving of minibuffer history to `savehist-file'. emacs builtin
    (desktop-save-mode 1)  ;; save desktop when exiting Emacs
    ))
(el-get-bundle! all-the-icons)
;;;; `geiser', `geiser-guile' and `guix'
;; If we do not use "elpa:geiser", el-get recipe for geiser points to http://git.sv.gnu.org/r/geiser.git
;; the gnu.org repo states that the rpoj has moved to gitlab. therefore the recipe fails to build untl someone
;; updates the recipe to point to gitlab
(el-get-bundle! elpa:geiser
  (progn (require 'geiser)))
;; el-get has a bug where the autoload from geiser-guile are put before those of geiser
;; this leads to funcall for `geiser-activate-implementation' before the autoload def of that function
;; occurs in the el-get/.loaddefs.el file
(el-get-bundle! geiser-guile
  (progn
    ;; because this is type elpa, el-get-do-init seems to not perform funcall to require
    ;; see lines 442-447 in el-get.el
    (require 'geiser-guile)
    (setopt geiser-guile-load-init-file t)))
(el-get-bundle! guix)
;;;; `delight'
(el-get-bundle! delight
  (progn
    ;; set outline-minor-mode indicator to toc icon
    ;; outline-minor-mode is provided by library `outline' so we need to explicitly mention that
    ;; otherwise nil for 3rd arg indicates that minor mode is provided as a separate feature through its own file
    ;; see https://www.emacswiki.org/emacs/DelightedModes
    (delight 'outline-minor-mode (concat " " (all-the-icons-material "toc")) 'outline)))

;;;; `magit' pulls dependencies: `compat', `dash', `transient' and `with-editor'
;; visualize and resolve git merge conflicts using feature `smerge-mode' which is emacs builtin
(el-get-bundle! magit) ;; initialization code in init-magit.el
;;;; `consult', `vertico', `orderless', `marginalia', `embark'
(el-get-bundle! consult) ;; initialization code in init-consult.el
(el-get-bundle! vertico) ;; ditto
(el-get-bundle! orderless) ;; ditto
(el-get-bundle! marginalia) ;; ditto
(el-get-bundle! embark) ;; ditto
(el-get-bundle! embark-consult
  :features (consult embark))
;;;; `which-key'
(el-get-bundle! which-key
  :features (embark)
  (progn (setq prefix-help-command #'embark-prefix-help-command)
         (which-key-mode 1)
         (delight 'which-key-mode nil t)))
;;;; `undo-tree'
(el-get-bundle! undo-tree
  (progn
    (global-undo-tree-mode)
    (delight 'undo-tree-mode (concat " " (all-the-icons-material "undo")) t)))

(el-get-bundle! poet-theme in poet
  (load-theme 'poet t))

(el-get-bundle! ace-window)
;;;; `s' the long lost Emacs string manipulation lib
(el-get-bundle! s)
;;;; `smartparens'
(el-get-bundle! smartparens
  :features (smartparens-config))
;;;; `transient'
;; magit requires package `transient' to display popups.
;; Allow using `q' to quit out of popups, in addition to `C-g'. See
;; <https://magit.vc/manual/transient.html#Why-does-q-not-quit-popups-anymore_003f>
(el-get-bundle! transient
  (transient-bind-q-to-quit))

;;;; `helpful' is an enhanced help
(el-get-bundle! helpful
  (bind-keys   ("C-h f"   . helpful-callable)
           ("C-h v"   . helpful-variable)
           ("C-h k"   . helpful-key)
           ("C-h C"   . helpful-command) ;; override describe-coding-system
           ("C-c C-d" . helpful-at-point)))

;;;; `expand-region' for selecting text objects
(el-get-bundle! expand-region
  (bind-keys ("C-=" . er/expand-region)))

;;;; `burly' for save and restore frame and window configurations (based on using bookmarks)
;; https://github.com/alphapapa/burly.el
(el-get-bundle! burly :type github :pkgname "alphapapa/burly.el" :info ".")
;;;; `org-mode'
(el-get-bundle! org-bullets)
(el-get-bundle! org in org-mode       ;; initialization code in init-org-mode.el
  :features (org-bullets ox-html ox-latex ox-md org-element))
;;;; `beancount-mode'
;; git@gitlab.com:milind.b.kamble/beancount-mode
(el-get-bundle! beancount in beancount-mode)
;;;; `markdown-mode'
(comment (el-get-bundle! markdown-mode))
;;;; `markdown-preview-mode'
(comment (el-get-bundle! markdown-preview-mode))
;;;; `eshell' customization
(eval-after-load 'esh-mode
  '(progn 
    (bind-key "M-." 'mbk/eshell-recall-args 'eshell-mode-map)))
(eval-after-load 'eshell
  '(progn  (require 'em-tramp)
           (setopt
            eshell-banner-message ""
            ;; eshell-scroll-to-bottom-on-input t
            eshell-error-if-no-glob t
            eshell-hist-ignoredups t
            eshell-save-history-on-exit t
            eshell-prefer-lisp-functions nil
            eshell-history-size 10000)
           ;; eshell-destroy-buffer-when-process-dies t
           ;; eshell-highlight-prompt nil

           (setenv "PAGER" "cat")
           
           ;; addding history-references to input-functions enable the use of !foo:n
           ;; to insert the nth arg of last command beg with foo
           ;; or !?foo:n for last command containing foo
           (add-hook 'eshell-mode-hook
                     (lambda ()
                       (add-to-list 'eshell-expand-input-functions 'eshell-expand-history-references)
                       (add-to-list 'eshell-modules-list 'eshell-rebind)
                       (add-to-list 'eshell-modules-list 'eshell-tramp))))
  )
;;;; `eshell-z'
(el-get-bundle! eshell-z
  :features (eshell)
  (setopt
   eshell-z-freq-dir-hash-table-file-name (concat eshell-directory-name "z-dir-table")))

;;;; `vc-hooks' optimization for speedup
;; As per Basti's blog https://bastibe.de/2014-05-07-speeding-up-org-publishing.html
;; removing vc-refresh-state from find-file-hook speeds up org-static-blog
(remove-hook 'find-file-hook 'vc-refresh-state)
(remove-hook 'kill-buffer-hook #'vc-kill-buffer-hook)

;;;; `qrencode' creates qrencoded image of any file content
(el-get-bundle! qrencode)
;;;; `free-keys' identify unbounded keys
(el-get-bundle! free-keys)
;;;; `yaml' parser in pure elisp
(el-get-bundle! yaml)
;;;; `yasnippet' and `yasnippet-snippets'
(el-get-bundle! yasnippet)
(el-get-bundle! yasnippet-snippets)
;;;; `keycast'
(el-get-bundle! keycast)
;;;; `csv-mode'
(el-get-bundle! csv-mode)
;;;; `sicp' structure and interpretation of computer programs
(el-get-bundle! sicp)
;;;; my utility funcs and custom bindings
(load "utils")
(load "customkeybindings")
(global-set-key (kbd "M-SPC") mbk/leader-map)
(when (file-exists-p custom-file) (load custom-file))


;; (makunbound 'mbk/outline-map2)
;; ((fmakunbound 'mbk/outline-map2)

;;;; to be considered
;; `ace-link'
;; `recentf-ext'
;; `sly' IDE for common lisp
;; `python-mode'
;; `markdown-mode'
;; `highlight-parentheses'
;; `corfu'
;; `yaml-pro-mode' a minor mode that probably augments the builtin major mode `yaml-ts-mode'
(comment

)
