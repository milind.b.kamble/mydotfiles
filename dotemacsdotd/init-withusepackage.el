;; ** Emacs initialization, customization from scratch        -*- lexical-binding: t; -*-
;; Copyright (C) 2021 by Milind Kamble

(setq mbk/full-name "Milind Kamble"
      mbk/email "milindbkamble@gmail.com")

;; help-map is a symbol whose symbol-value is keymap
;; help-command is a symbol whose function-value is the same help-map

;; In elisp there is a ignore function, but that evaluates its argument, yielding always nil, whereas comment doesn't evaluate its body. It's useful to temporary disable bits of code.
;; source: https://www.omarpolo.com/dots/emacs.html
(defmacro comment (&rest _body)
  "Ignore BODY, just like `ignore', but this is a macro."
  '())

;; *** `use-package'
;; keywords: config -- for code to run after pacakge is loaded, init -- for code to run before pacakge is loaded
(eval-when-compile (require 'use-package))
(require 'use-package-ensure)
(setq use-package-always-ensure nil)  ;; do not install missing packages
(setq use-package-always-defer t)

;; *** `emacs' native settings
;; see https://www.reddit.com/r/emacs/comments/mk9ehd/curious_whats_the_use_of_usepackage_emacs/
;; use-package emacs is a special way to keep your settings unrelated to any package.
(use-package emacs
  :custom
  ((require-final-newline t)
   ;; ignore case for completions
   (completion-ignore-case t)
   (read-file-name-completion-ignore-case t)
   (read-buffer-completion-ignore-case t)
   (auto-save-visited-mode +1)
   (auto-save-visited-interval 5)
   (recentf-max-saved-items 3000))
  :config
  (fset 'yes-or-no-p 'y-or-n-p)
  (setq indent-tabs-mode nil) ; use spaces instead of tabs
  (setq isearch-lazy-count t  ; Some very small tweaks for isearch
	search-whitespace-regexp ".*?"
	isearch-allow-scroll 'unlimited)
  ;; desktop session save settings
  (setq desktop-restore-frames nil)  ;; uses frame parameters (fonts, faces etc) from init file instead from previous session
  (desktop-save-mode 1)  ;; aito save desktop when exiting Emacs
  (setq password-cache-expiry 86400) ; 24hrs before sudo password is prompted again
  
  ;; :init -- I think all the customization can be done in config instead of init
  
  ;; ux elements
  (set-face-attribute 'default nil :family "Victor Mono" :height 120)
  ;; As per https://protesilaos.com/codelog/2020-09-05-emacs-note-mixed-font-heights/
  ;; we need to specify height of other fonts relative to default for text-scaling to work
  ;; implicit height does not work, we need to explicitly specify the :height 1.0
  (set-face-attribute 'fixed-pitch nil :family "Victor Mono" :height 1.0)
  (set-face-attribute 'variable-pitch nil :family "Source Sans 3" :height 1.0) ;; for proportional or dynamic font width
  (variable-pitch-mode -1) ;; disable varibale-pitch by default
  (defalias 'proportional-font-mode 'variable-pitch-mode
    "porportional-font is easier to remember than variable-pitch") ;; my own alias since I tend to forget `variable-pitch-mode'

  (defalias 'toggle-wrap-lines 'toggle-truncate-lines
    "wrap-lines is easier to remember than truncate-lines") ;; an easier to remember alias for toggling line wrapping
  
  ;; begin create a hook to run after load-theme so that we can customize some faces in the theme
  (defvar mbk/after-load-theme-hook nil
    "Hook run after a color theme is loaded using `load-theme'.")
  (defadvice load-theme (after mbk/run-after-load-theme-hook activate)
    "Run `after-load-theme-hook'."
    ;; debug (message (format "mbk: calling run-hooks. mbk/after-load-theme-hook=%s" mbk/after-load-theme-hook))
    (run-hooks 'mbk/after-load-theme-hook))
  (defun mbk/customize-poet ()
    "Customize poet theme"
    (if (member 'poet custom-enabled-themes) ;; use `progn' to add debug msg for `then' clause
	(custom-theme-set-faces 'user  ;; need to use `user'. `poet is overridden by `user''
				'(font-lock-comment-face
				  ((t (:slant italic :weight normal))))))) ;; weight = thin|normal|heavy
  ;; end create after load-theme hook
  ;; modeline customization
  ;;**** customize modeline-modified indicator using chain icon 
  ;; mode-line-modified is a mode line construct for displaying whether current buffer is modified.
  ;; we beautify it with faicon (fa means fontawesome family)
  ;; see also https://github.com/domtronn/all-the-icons.el/wiki/Mode-Line
  (defun mbk/modeline-modified ()
    (let* ((config-alist   ;; hash like datastructure. key . (family icon-select-func parameter-for-icon-select)
	    ;; format-mode-line returns a string representing modified status of current buffer
	    ;; "*" means modifed, "-" means unmodified, "%" means read-on;y
            '(("*" all-the-icons-faicon-family all-the-icons-faicon
               "chain-broken" :height 1.2 :v-adjust -0.0)
              ("-" all-the-icons-faicon-family all-the-icons-faicon
               "link" :height 1.2 :v-adjust -0.0)
              ("%" all-the-icons-octicon-family all-the-icons-octicon
               "lock" :height 1.2 :v-adjust 0.1)))
           (result (cdr (assoc (format-mode-line "%*") config-alist))))

      (propertize (format "%s" (apply (cadr result) (cddr result)))
                  'face `(:family ,(funcall (car result)) :inherit ))))
  (setq-default mode-line-modified '(:eval (mbk/modeline-modified)))
  ;;**** indiccate major-mode by suitable icon
  ;; mode-line-client-mode is ':' for normal frames, @ for emacsclient frames
  ;;**** major mode indicator is constructed by 'mode-name'
  ;; we can change the font to italic by customizing mode-line-buffer-id
  (defun mbk/modeline-mode-name ()
    (let* ((icon (all-the-icons-icon-for-mode major-mode))
	   (face-prop (and (stringp icon) (get-text-property 0 'face icon))))
      (when (and (stringp icon) (not (string= major-mode icon)) face-prop)
	(setq mode-name (propertize icon 'display '(:ascent center))))))
  (add-hook 'after-change-major-mode-hook #'mbk/modeline-mode-name)
  (add-hook 'find-file-hook #'repeat-mode) ; enabling repeat mode for all files visited
  (load "utils")
  (load "customkeybindings")
  (global-set-key (kbd "M-SPC") mbk/leader-map)
  (global-set-key [remap dabbrev-expand] 'hippie-expand) ;; from https://www.masteringemacs.org/article/text-expansion-hippie-expand
  (with-eval-after-load 'dired (bind-key "C-c C-e" 'dired-toggle-read-only 'dired-mode-map))  ; C-x C-q is taken over by view-mode, so choosing this alternate binding
  (defalias 'dired-edit-in-place 'dired-toggle-read-only "rename files by editing dired like a regular buffer")
  (setq-default
   ispell-program-name "enchant-2"
   ;; TAB cycle if there are only few candidates. If there are fewer than threshold candidates, TAB cycles through them without popup
   completion-cycle-threshold 3
   ;; Enable indentation+completion using the TAB key.
   ;; `completion-at-point' is often bound to M-TAB.
   tab-always-indent 'complete
   ;; sort-fold-case useful in sorting lines in region with case-fold (ie case insensitive)
   sort-fold-case t)
  (add-to-list
   'load-path (expand-file-name "lisp" user-emacs-directory))
  ) ; end of use-package emacs

;; imenu is a mean of navigation in a buffer. It can act like a TOC, for instance

;; *** `blackout'
(use-package blackout
  :demand t)

;; *** `all-the-icons'
(use-package all-the-icons :if (display-graphic-p))
(set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'prepend)
(set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'prepend)

;; *** `elpy'
;; Elpy brings powerful Python editing to Emacs.  It combines and
;; configures a number of other packages written in Emacs Lisp as well as
;; Python, together offering features such as navigation, documentation,
;; completion, interactive development and more.
;; mbk: i'm not ready for using it yet. elpy needs company.el, and I am yet to figure out
;; the functionality of company in relation to consult, vertico and embark. there is also a company alternative called corfu
;; (use-package elpy
;;   :ensure t
;;   :defer t
;;   :init
;;   (advice-add 'python-mode :before 'elpy-enable))

;; *** `csv-mode' - keep files inside user-emacs-directory 

;; *** `no-littering' - keep files inside user-emacs-directory 
(use-package no-littering
  :demand t
  :commands (no-littering-expand-var-file-name
             no-littering-expand-etc-file-name)
  :config
  ;; (add-to-list 'recentf-exclude no-littering-var-directory)
  ;; (add-to-list 'recentf-exclude no-littering-etc-directory)
  (setq
   auto-save-file-name-transforms     ; transform names of saved files
   `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))
   backup-directory-alist ; store all backup and autosave files in "backups"
   `((".*" . ,(no-littering-expand-var-file-name "backups/")))
   custom-file (no-littering-expand-etc-file-name "custom.el")
   recentf-max-saved-items 3001) ; since we modify the recentf-save-file, we wait until no-littering has made those changes then enable recentf-mode which then loads the recentf-save-file
  (recentf-mode 1))

;; *** `dabbrev'
;; Use Dabbrev with Corfu!
(use-package dabbrev
  ;; Other useful Dabbrev configurations.
  :custom
  (dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'"))) ; \(?:regex\) This is called shy group. Group for precedence, but no capture.

;; *** `ispell' (TBD: switch to enchant)
;; unable to setup correctly. emacs-ispell communicates with aspell process via two-way pipe
;; exchanging commands and results. Somehow the personal and replacement files names in the
;; config of the emacs-invoked aspell is different than the standalone aspell process
;; see more details in the notes related to aspell
(use-package ispell
  ;; :init
  ;; (add-hook 'ispell-initialize-spellchecker-hook (lambda () (setq ispell-current-dictionary "en_US")))
  )

	     
;; ** Version control
;; *** disable native VC backends since we will use magit 
;; Feature `vc-hooks' provides hooks for the Emacs VC package. We
;; don't use VC, because Magit is superior in pretty much every way.
(use-package vc-hooks
  :config

  ;; Disable VC. This improves performance and disables some annoying
  ;; warning messages and prompts, especially regarding symlinks. See
  ;; https://stackoverflow.com/a/6190338/3538165.
  (setq vc-handled-backends nil)
  ;; As per Basti's blog https://bastibe.de/2014-05-07-speeding-up-org-publishing.html
  ;; removing vc-refresh-state from find-file-hook speeds up org-static-blog
  (remove-hook 'file-find-hook #'vc-refresh-state)
  (remove-hook 'kill-buffer-hook #'vc-kill-buffer-hook))

;; *** visualize and resolve git merge conflicts using feature `smerge-mode'
;; smerge-mode is part of std installation under "vc" subdir
(use-package smerge-mode
  :blackout t)

;; *** use emacs as external editor through package `with-editor'
(use-package with-editor)

;; *** `helpful' is an enhanced help
(use-package helpful
  :demand t
  :bind
  ("C-h f" . #'helpful-callable)
  ("C-h v" . #'helpful-variable)
  ("C-h k" . #'helpful-key)
  ("C-c C-d" .  #'helpful-at-point)
  ("C-h C" . #'helpful-command) 	; override describe-coding-system
  )

;; *** bookmark
(use-package bookmark
  ;; :bind (("C-z b b" . bookmark-jump)   C-z needs to be a keymap. presently it is bound to command avy-goto-char-2
  ;;        ("C-z b a" . bookmark-set)
  ;;        ("C-z b l" . list-bookmarks))
  )

;; *** `ace-window' and `ace-link' 
(use-package ace-window
  :custom
  (aw-keys '(?a ?o ?e ?u ?h ?t ?n ?s))
  :bind
  (("M-o" . #'ace-window) ;; bind in global-map
   ("M-O" . #'switch-to-buffer-other-window)))

(use-package ace-link
  :config (ace-link-setup-default))

;; *** `recentf'
(comment use-package recentf
  :blackout t
  :custom
  (recentf-max-saved-items 3000)
  :config
  (recentf-mode +1)
  (recentf-load-list))

;; *** `recently' for accessing recently visited files 
(comment use-package recently
  :blackout t
  :custom
  (recently-max 3000)
  :bind
  ("C-x C-r" . #'recently-show)
  :config
  (recently-mode +1))

;; *** sly - common-lisp IDE
(use-package sly
  :custom
  ;; (sly-default-lisp 'sbcl)
  (inferior-lisp-program "sbcl"))

;; *** magit requires package `transient' to display popups.
(use-package transient
  :demand t
  :config

  ;; Allow using `q' to quit out of popups, in addition to `C-g'. See
  ;; <https://magit.vc/manual/transient.html#Why-does-q-not-quit-popups-anymore_003f>
  ;; for discussion.
  (transient-bind-q-to-quit))

(use-package magit
  :preface
  (defun magit-dired-other-window ()
    (interactive)
    (dired-other-window (magit-toplevel)))
  :commands (magit-clone
             magit-toplevel
             magit-read-string-ns
             magit-remote-arguments
             magit-get
             magit-remote-add
             magit-define-popup-action)
  :bind (
	 ;; already defined("C-x g" . #'magit-status)
	 ;; already defined("C-x M-g" . #'magit-dispatch)
	 ;; already defined("C-c M-g" . #'magit-file-dispatch)
         :map magit-mode-map
               ("C-o" . magit-dired-other-window)
	       )
  :init
  (setq-default magit-diff-refine-hunk t)
  ;; (defvar magit-last-seen-setup-instructions "1.4.0")
  ;; include gpg-sign infix command https://github.com/magit/magit/issues/3771
  (setq transient-default-level 5)
  (message (format "set transient-default-level=%d" transient-default-level))
  :config
  (fullframe magit-status magit-mode-quit-window))

;; beancount-minor-mode deprecated because beancount-mode (major mode is more feature rich)
;; *** beancount-minor-mode
;; (use-package beancount-minor-mode
;;   :blackout (beancount-minor-mode . " bcn")
;;   :commands (bc-enable-if-filename-match
;; 	     bc-forward-transaction
;; 	     bc-backward-transaction)
;;   :bind
;;   (:map beancount-minor-mode-map
;; 	("C-S-n" . #'bc-forward-transaction)
;;("C-S-p" . #'bc-backward-transaction)))
;; ** beancount-mode
(defun beancount-goto-prev-transaction (&optional arg)
  "Move to the prev transaction. Optional argument ignored for now"
  (interactive)
  (beancount-goto-transaction-begin)
  (re-search-backward beancount-transaction-regexp nil t))
(defun mbk/beancount-goto-transaction-begin ()
  "Move the cursor to the first line of the transaction definition."
  (interactive)
  (beginning-of-line)
  (if (looking-at-p "$") (forward-line -1))
  ;; everything that is indented with at lest one space or tab is part
  ;; of the transaction definition
  (while (looking-at-p "[ \t]+")
    (forward-line -1))
  (point))
(use-package mbk-beancount
  :after consult
  :custom
  (beancount-use-ido nil)
  :mode (("\\.beancount\\'" . beancount-mode))
  :hook ((beancount-mode . outline-minor-mode))   ; investigate using allout minor mode instead of outline-minor
  :bind-keymap ("C-c C-t" . outline-mode-prefix-map)
  :bind
  (:map beancount-mode-map
	("C-c C-n" . #'beancount-goto-next-transaction)
	("C-c C-p" . #'beancount-goto-prev-transaction)
	("C-c C-a" . #'mbk/beancount-goto-transaction-begin)
	("C-c C-e" . #'beancount-goto-transaction-end)
	("C-c '" . #'consult-beancount-account)
	("M-["     . #'consult-beancount-account))
  (:repeat-map my/beancount-mode-map-repeat-map
	       ("n" . beancount-goto-next-transaction)
	       ("p" . beancount-goto-prev-transaction)
	       ("a" . beancount-goto-transaction-begin)
	       ("e" . beancount-goto-transaction-end))
  :config
  (defun bc-simple-dt (dt)
    "Read a string of form [[YEAR] MM] DD and convert it to beancount compliant date. single digits are appropriately padded. Default value for YEAR is file-variable `bc-year' or 2000 and for MM is 01"
    (interactive "M")
    ;; (message "dt=%s" dt)
  (defun mth-dt-pad0 (mth-or-dt)
    (substring (concat "00" mth-or-dt) -2 nil))
  (defun yr-pad (yr)
    (concat "20" (mth-dt-pad0 yr)))
  (pcase (split-string dt)
    (`(,y ,m ,d) (format "%s-%s-%s" (yr-pad y) (mth-dt-pad0 m) (mth-dt-pad0 d)))
    (`(,m ,d) (format "%s-%s-%s" (if (boundp 'bc-year) bc-year "2000") (mth-dt-pad0 m) (mth-dt-pad0 d)))
    (`(,d) (format "%s-%s-%s" (if (boundp 'bc-year) bc-year "2000") (mth-dt-pad0 d)))
    (_ (error "unsupported input form")))))

(use-package savehist  ;; recommended by vertico author
  :init (savehist-mode))

;; *** configure `which-key'
;; setting prefix-help-command to my  preferred version based on reading at
;; https://with-emacs.com/posts/ui-hacks/prefix-command-completion/
(use-package which-key
  :after embark
  ;; :demand t
  :blackout t
  ;; :hook
  ;; (which-key-init-buffer . my--wk-help-dbg)
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  (which-key-mode 1))

;; *** `consult', `embark', `marginilia', `orderless', `avy'
;; this collection of tools provides completion featrues
;; `consult' provides completion
;; `marginilia' provides annotation of choices
;; `embark' provides contextual actions for each candidate
;; `orderless' provides search terms in any order and probably fuzzi-search too
(load "consult-and-friends")

;; *** `undo-tree' is a more intuitive way to navigate undo instead of linear traversal
(use-package undo-tree
  :blackout t
  :bind (("C-x u" . undo-tree-visualize))
  :init (global-undo-tree-mode))

;; *** `expand-region' for selecting text objects
(use-package expand-region
  :bind (("C-=" . er/expand-region)))

;; *** `python-mode'
(use-package python-mode
  :bind (:map python-mode-map
	      ("C-c C-n" . mbk/elpy-nav-forward-block) ;; move to next line indented like point
	      ("C-c C-p" . mbk/elpy-nav-backward-block) ;; move to next line indented like point
	 ))
;; *** `smartparens' handles paired punctuations The auhor recommends
;; initialization using (require 'smartparens-config), but we want to
;; do so using use-package. Found the solution 
;; [[https://www.wisdomandwonder.com/article/9897/use-package-smartparens-config-ensure-smartparens][here]]
(use-package smartparens
  :blackout t
  :bind (:map smartparens-mode-map
	      ;; see variable `sp-smartparens-bingdings' in package source `smartparens.el'
					; use default ("C-M-a" . sp-beginning-of-sexp) 
					; use default ("C-M-e" . sp-end-of-sexp)
					; use default ("C-(" . sp-forward-barf-sexp)
					; use default ("C-)" . sp-forward-slurp-sexp)
					; use default ("C-{" . sp-backward-barf-sexp)
					; use default ("C-}" . sp-backward-slurp-sexp)
	      
              ("C-k" . sp-kill-hybrid-sexp)
	      ("C-," . sp-rewrap-sexp)
	      :map emacs-lisp-mode-map (";" . sp-comment)
	      :map lisp-mode-map (";" . sp-comment)
	      )
  :config
  (setq sp-show-pair-from-inside nil)
  (require 'smartparens-config)
  (smartparens-global-mode)
  (sp-with-modes 'org-mode
    (sp-local-pair "=" "=" :wrap "C-="))
  (bind-key [remap c-electric-backspace] #'sp-backward-delete-char
            smartparens-strict-mode-map)
  ;; (unbind-key "M-<delete>" smartparens-mode-map) ; M-<delete> will perform sp-unwrap-sexp (forward looking)
  (unbind-key "M-<backspace>" smartparens-mode-map) ; I prefer M-<backspace> to perform backward-kill-word

  :hook ((prog-mode . turn-on-smartparens-strict-mode)
         (web-mode . op/sp-web-mode)
         (LaTeX-mode . turn-on-smartparens-strict-mode))
  :custom ((sp-highlight-pair-overlay nil)
	   (sp-base-key-bindings 'sp)))

(use-package highlight-parentheses
  :blackout t
  :config (global-highlight-parentheses-mode))


;; *** `markdown-mode'
(use-package markdown-mode
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'" . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown")
  :hook
  (markdown-mode . variable-pitch-mode))

;; *** `org-static-blog'
(use-package org-static-blog
  :config
  (setq org-static-blog-publish-title "Milind Kamble Blog")
  (setq org-static-blog-publish-url "https://webhosttry1.web.app")
  (setq org-static-blog-publish-directory "~/opensource/myblog-orgstatic/public/")
  (setq org-static-blog-posts-directory  "~/opensource/myblog-orgstatic/posts/")
  (setq org-static-blog-drafts-directory  "~/opensource/myblog-orgstatic/drafts/")
  (setq org-static-blog-enable-tags t)
  (setq org-export-with-toc nil)
  (setq org-export-with-section-numbers nil)

  ;; This header is inserted into the <head> section of every page:
  ;;   (you will need to create the style sheet at
  ;;    ~/projects/blog/static/style.css
  ;;    and the favicon at
  ;;    ~/projects/blog/static/favicon.ico)
  (setq org-static-blog-page-header
	"<meta name=\"author\" content=\"Milind Kamble\">
<meta name=\"referrer\" content=\"no-referrer\">
<link href= \"static/style.css\" rel=\"stylesheet\" type=\"text/css\" />
<link rel=\"icon\" href=\"static/favicon.ico\">")

  ;; This preamble is inserted at the beginning of the <body> of every page:
  ;;   This particular HTML creates a <div> with a simple linked headline
  (setq org-static-blog-page-preamble
	"<div class=\"header\">
  <a href=\"https://webhosttry1.web.app\">Milind's Blog</a>
</div>")
n
  ;; This postamble is inserted at the end of the <body> of every page:
  ;;   This particular HTML creates a <div> with a link to the archive page
  ;;   and a licensing stub.
  (setq org-static-blog-page-postamble
	"<div id=\"archive\">
  <a href=\"https://webhosttry1.web.app/archive.html\">Older posts</a>
</div>
<center><a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/3.0/\"><img alt=\"Creative Commons License\" style=\"border-width:0\" src=\"https://i.creativecommons.org/l/by-sa/3.0/88x31.png\" /></a><br /><span xmlns:dct=\"https://purl.org/dc/terms/\" href=\"https://purl.org/dc/dcmitype/Text\" property=\"dct:title\" rel=\"dct:type\">bastibe.de</span> by <a xmlns:cc=\"https://creativecommons.org/ns#\" href=\"https://bastibe.de\" property=\"cc:attributionName\" rel=\"cc:attributionURL\">Bastian Bechtold</a> is licensed under a <a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/3.0/\">Creative Commons Attribution-ShareAlike 3.0 Unported License</a>.</center>")

  ;; This HTML code is inserted into the index page between the preamble and
  ;;   the blog posts
  (setq org-static-blog-index-front-matter
	"<h1> Welcome to my blog </h1>\n"))

;; *** EPA
(setq
 epa-armor t
 epa-file-name-regexp "\\.\\(gpg\\|asc\\)$"
 epg-pinentry-mode 'ask	   ; use minibuffer reading passphrase
 epa-file-encrypt-to '("milind.b.kamble@gmail.com"))
(epa-file-name-regexp-update)

;; *** `s' the (long lost) emacs string manipulation library
(use-package s
  :commands (s-replace))

;; *** `org-mode' itself 
(use-package org
  ;; :mode ("\\.org\\'" . org-mode)
  ;;tobedeleted :mode ("\\.beancount\\(\\.asc\\)?\\'" . org-mode)
  ;; :diminish org-indent-mode
  :config
  (setq org-agenda-files "~/notes/agenda.org")
  (setq org-directory "~/notes")
  (require 'ox-html)
  (require 'ox-latex)
  (require 'ox-md)
  (require 'org-element)
  (setq org-startup-indented t
	org-adapt-indentation nil
	org-edit-src-content-indentation 0
	org-outline-path-complete-in-steps nil
	org-refile-use-outline-path 'full-file-path
	org-refile-allow-creating-parent-nodes 'confirm
	;; org-refile-targets `((nil . (:maxlevel . 3)
	;; 			  (org-agenda-files . (:maxlevel . 3))))
	org-ellipsis "⤵"   ;; alternate: " ▾" or "◦◦◦"
	org-hide-emphasis-markers t
	org-log-into-drawer t
	org-directory org-directory
	org-default-notes-file (concat org-directory "/todo.org"))
  (org-babel-do-load-languages
   'org-babel-load-languages  ;; many more exist
   '((python . t)
     (emacs-lisp . t)
     (shell . t)
     (lisp . t)
     (gnuplot . t)
     (R . t)
     (C . t)))
  (setq org-src-window-setup 'current-window)
  (setq org-agenda-window-setup 'current-window)
  :hook
  (
   ;; (org-mode . bc-enable-if-filename-match)
   (org-mode . variable-pitch-mode)
   ;; (org-mode . flyspell-mode)
   ))

;; *** narrow-dwim
(use-package eshell-z
  :after eshell
  :custom
  (eshell-z-freq-dir-hash-table-file-name
   (concat eshell-directory-name "z-dir-table"))
  (eshell-history-size 10000))

;; `outdent' check if this mode is useful. It is advertised for python-like indentation based
;; source code browsing . https://do.myriadicity.net/software-and-systems/craft/emacs-sundries/outdent.el

;; `eshell' utilities
;; other eshell related packxages
;; eshell-autojump  the command j to list common directories and to jump to them.
;; `eshell-bookmark' a simple package integrating eshell with bookmark.el.
;; `eshell-fixedprompt' Minor mode to restrict eshell to use a single fixed prompt
;;     wherein the prev command output isremoved (or scrolled out of sightG)uu;
;; `eshell-outline' Enhanced outline-mode for Eshell
;; `eshell-syntaxhighlighting'
;; `eshell-up' navigating to a specific parent directory in eshell without typing ../.. etc
(use-package eshell
  ;; :bind (:map eshell-mode-map
  ;;             ([remap eshell-pcomplete] . helm-esh-pcomplete)
  ;;             ("M-r" . helm-eshell-history)
  ;;             ("M-s f" . helm-eshell-prompts-all))
  ;; :bind (:map eshell-mode-map
  ;; 	      ("C-c h" . consult-history)) ;; already bound in use-package consult
  :custom
  (eshell-banner-message "")
  ;; (eshell-scroll-to-bottom-on-input t)
  (eshell-error-if-no-glob t)
  (eshell-hist-ignoredups t)
  (eshell-save-history-on-exit t)
  (eshell-prefer-lisp-functions nil)
  ;; addding history-references to input-functions enable the use of !foo:n
  ;; to insert the nth arg of last command beg with foo
  ;; or !?foo:n for last command containing foo
  (add-to-list 'eshell-expand-input-functions 'eshell-expand-history-references)
  (add-to-list 'eshell-modules-list 'eshell-rebind)
  (add-to-list 'eshell-modules-list 'eshell-tramp)
  ;; (eshell-destroy-buffer-when-process-dies t)
  ;; (eshell-highlight-prompt nil)

  :config
  (setenv "PAGER" "cat")
  (require 'eshell-z)
  (require 'em-tramp))


;; Being able to narrow to the output of the command at point seems very useful
(defun mbk/eshell-narrow-to-output ()
  "Narrow to the output of the command at point."
  (interactive)
  (save-excursion
    (let* ((start (progn (eshell-previous-prompt 1)
                         (forward-line +1)
                         (point)))
           (end (progn (eshell-next-prompt 1)
                       (forward-line -1)
                       (point))))
      (narrow-to-region start end))))
(defun mbk/setup-eshell ()
  (define-key eshell-mode-map (kbd "C-c M-l") #'mbk/eshell-narrow-to-output))

;; *** literate calc-mode. Do math in line.
;; (add-hook 'eshell-mode-hook
;; 	   (lambda ()
;; 	     (local-set-key (kbd "C-c h")
;; 			    (lambda ()
;; 			      (interactive)
;;	 		      (insert
;; 			       (ido-completing-read "Eshell history: " ;; there must be something equivalent for consult or vertico
;; 						    (delete-dups
;; 						     (ring-elements eshell-history-ring))))))
;; 	     ))


;; ** TBD
;; dired
;; occur, loccur
;; hideshow
;; gemini mode -- related to blog authoring?
;; olivetti -- Olivetti mode "centers" the buffer, it's nice when writing text (gemini-mode, markdown-mode etc.)
;; my-abbrev is a package-like file to store our custom abbreviations
;; eglot
;; as per OmarPolo : There are two major implementations for emacs: lsp-mode and eglot. lsp-mode is too noisy for me, I prefer eglot as it's less intrusive
;; Emacs-lisp doesn't have namespaces, so usually there's this convention of prefixing every symbol of a package with the package name. Nameless helps with this. It binds _ to insert the name of the package, and it visually replace it with :. It's pretty cool.
;; lua-lsp is the LSP server for lua, to hook it into eglot :
;; (with-eval-after-load 'eglot
;;   (add-to-list 'eglot-server-programs
;;                '((lua-mode) . ("lua-lsp"))))
;; nov is a package to read epub from Emacs. It's really cool, and integrates with both bookmarks and org-mode
;; pq is a postgres library module for elisp.

;; *** `view mode'. Sometimes it's handy to make a buffer read-only. Also, define some key to easily navigate in read-only buffers.
(use-package view
  :bind (("C-x C-q" . view-mode)
         :map view-mode-map
         ("n" . next-line)
         ("p" . previous-line)
         ("l" . recenter-top-bottom)))

;; *** `geiser'
(use-package geiser-guile
  :config
  (setq geiser-guile-load-path
        (list (expand-file-name "~/opensource/mydotfiles/dotconfig/guix/my-modules")
	      (expand-file-name "~/.config//guix/current/share/guile/site/3.0")))
  ;; workaround for guix https://gitlab.com/emacs-guix/emacs-guix/-/issues/30
  (defun geiser-company--setup (x) nil)
  (setq geiser-repl-company-p nil))

;; *** `eglot'    ;; see https://github.com/joaotavora/eglot#how-does-eglot-work
(use-package eglot ;; from https://github.com/clojure8/emacs0/tree/main/modules/lang
  :commands (eglot)
  :bind
  (:map eglot-mode-map
        ("s-<return>" . eglot-code-actions)
        )
  :config
  (setq eglot-connect-timeout 10)
  (setq eglot-sync-connect t)
  (setq eglot-autoshutdown t))

;; *** `consult-eglot'   ;; https://github.com/mohkale/consult-eglot
(use-package consult-eglot
  :after eglot
  :commands (consult-eglot))

;; *** `hercules' is a which-key based hydra
(use-package hercules
  :commands
  (hercules-def))

;; *** `free-keys' shows unbound keys
(use-package free-keys)

;; *** `yaml'
(use-package yaml-mode
  :mode "\\.yml\\'"
  :hook ((yaml-mode . turn-off-flyspell)))

;; *** `text-mode'
(use-package text-mode
  :hook (
	 ;; (text-mode . flyspell-mode)
	 (text-mode . abbrev-mode)
	 (text-mode . variable-pitch-mode)))

;; *** `yasnippet'
(use-package yasnippet
  :config
  (yas-global-mode 1)
  )

;; ** my customization
;; *** UX
(use-package poet-theme
  :init
  (add-hook 'mbk/after-load-theme-hook #'mbk/customize-poet)  ; need to add-hook before loading theme
  (load-theme 'poet t)    ; see `custom-available-themes' for optional themes
  )

;; change the `lighter' of visual-line-mode to an icon
(use-package simple
  :after all-the-icons
  :config (global-visual-line-mode +1)  ;; enable visual-line-mode everywhere
  :blackout (visual-line-mode . (concat " " (all-the-icons-material "wrap_text"))))
;; Atlernate method
;; (with-eval-after-load 'all-the-icons
;;   (setcar (cdr (assq 'visual-line-mode minor-mode-alist))
;; 	  (concat " " (all-the-icons-material "wrap_text"))))

(use-package dired-recent
  :config
  (dired-recent-mode 1))

;; (use-package speed-type)



(when (file-exists-p custom-file) (load custom-file))
