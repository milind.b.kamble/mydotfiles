;; -*- lexical-binding: t; -*-
(use-package consult
  :bind
  (
   ;; these are all bound in global-map
   ;; since ctl-x-map is bound to C-x in global-map there is a key sequence in the global-map to each of the following commands listed after the remap. Hence our remapping to consult-flavored equivalents works
   ([remap switch-to-buffer] . consult-buffer)
   ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
   ([remap yank-pop] . consult-yank-pop)
   ([remap bookmark-jump] . consult-bookmark)
   ([remap repeat-complex-command] . consult-complex-command)
   ([remap project-switch-to-buffer] . consult-project-buffer)
   ([remap goto-line] . consult-goto-line)
   ([remap next-matching-history-element] . consult-history)
   ([remap prev-matching-history-element] . consult-history)
   ([remap isearch-edit-string] . consult-isearch-history)
   ("C-c h" . consult-history) ; access current buffer history. useful for eshell, comint. useable even in mini-buffer
   ("C-c i" . consult-info)
   ("C-c M-x" . consult-mode-command) ; like M-x but filter command related to mojor mode
   ("C-c k" . consult-kmacro) ; exec a macro from macro-ring
   ("M-#" . consult-register-load)
   ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
   ("C-M-#" . consult-register)
   :map project-prefix-map
   ("i" . consult-project-imenu)
   :map goto-map
   ;; goto-map is defined in bindings.el.gz and by default only the following are setup
   ;; c . goto-char
   ;; g . goto-line
   ;; n . next-error
   ;; p . previous-error
   ;; TAB . move-to-column
   ("e" . consult-compile-error)
   ("i" . consult-imenu)
   ("I" . consult-imenu-multi)
   ("k" . consult-global-mark)
   ("l" . consult-line)
   ("m" . consult-mark)
   ("o" . consult-outline)
   :map search-map
   ("G" . consult-git-grep)
   ("L" . consult-line-multi)
   ("b" . flush-blank-lines)
   ("d" . consult-find)
   ("e" . consult-isearch-history) ; Isearch integration
   ("g" . consult-grep)
   ("k" . consult-keep-lines)
   ("l" . consult-line)
   ("r" . consult-ripgrep)
   ("u" . consult-focus-lines)
   :map isearch-mode-map
   ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
   ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
   ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
   ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
   ;; Minibuffer history
   :map minibuffer-local-map
   ("M-s" . consult-history)                 ;; orig. next-matching-history-element
   ("M-r" . consult-history)                ;; orig. previous-matching-history-element

   :map help-map
   ("M" . consult-man))
  :custom ((register-preview-delay 0)
           (register-preview-function #'consult-register-format)
	   (consult-narrow-key "<")
           )
  :config
  ;; the following customization to prevent auto-preview and instead use manual preview came from
  ;; README.org file of consult (https://github.com/minad/consult) -- loading recent files or bookmarks may result in expensive operations, so do manual preview
  (consult-customize
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key "M-.")
  ;; (defun consult-beancount-account-new (&optional arg)
  ;;   (interactive "p")
  ;;   (let ((acct (consult--read #'mbk/beancount-account-completion-table :prompt "Acct: " :predicate nil :require-match t))))
  ;;   (insert acct))
  (defun consult-beancount-account (&optional arg)
    "Insert a beancount account. Universal or > 1 prefix will refresh the candidate list"
  ;; "p" gives us numeric form of prefix
  ;;  arg = 1 if prefix eq nil
  ;;       = -1 if prefix is '-' (M-- or C-u -)
  ;;       = -n if prefix is -n (M-- n or C-u -n) 
  ;;       = +n if prefix is n (M-n or C-u n)
  ;;       = 4, 16, 64 etc if prefix is repeated C-u
  ;; so if arg > 1 or beancount-accounts is null, we refresh beancount-accounts
    (interactive "p")
    (if (> arg 0) (progn
		    (message "buufer=%s beancount-accounts.len=%d"
			     (current-buffer)
			     (length beancount-accounts))
		    (setq beancount-accounts nil)))
    (let ((acct (consult--read
		 (consult--completion-table-in-buffer #'beancount-account-completion-table)
		 :prompt "Acct: " :predicate nil :require-match t)))
      (insert acct)))
  ;; closely based on `consult--source-recent-file' but using `recently'
  ;; (defvar consult--source-recently-file
  ;; `(:name     "File"
  ;;   :narrow   ?z
  ;;   :category file
  ;;   :face     consult-file
  ;;   :history  file-name-history
  ;;   :state    ,#'consult--file-state
  ;;   :new      ,#'consult--file-action
  ;;   :enabled  ,(lambda () recently-mode)
  ;;   :items
  ;;   ,(lambda ()
  ;;      (let ((ht (consult--buffer-file-hash))
  ;;            items)
  ;;        (dolist (file (bound-and-true-p recentf-list) (nreverse items))
  ;;          ;; Emacs 29 abbreviates file paths by default, see
  ;;          ;; `recentf-filename-handlers'.  I recommend to set
  ;;          ;; `recentf-filename-handlers' to nil to avoid any slow down.
  ;;          (unless (eq (aref file 0) ?/)
  ;;            (let (file-name-handler-alist) ;; No Tramp slowdown please.
  ;;              (setq file (expand-file-name file))))
  ;;          (unless (gethash file ht)
  ;;            (push (consult--fast-abbreviate-file-name file) items))))))
  ;; "Recent file candidate source for `consult-buffer'.")
  ;; (delq 'consult--source-recent-file consult-buffer-sources)
  ;; (add-to-list 'consult-buffer-sources 'consult--source-recently-file 'append)
  )

(comment (use-package consult  ;; source: https://www.omarpolo.com/dots/emacs.html
  :bind (("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c b" . consult-bookmark)
         ("C-c k" . consult-kmacro)
         ("C-x M-:" . consult-complex-command)
         ("C-x b" . consult-buffer)
         ("C-x 4 b" . consult-buffer-other-window)
         ("C-x 5 b" . consult-buffer-other-frame)
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)
         ("C-M-#" . consult-register)
         ("M-g e" . consult-compile-error)
         ("M-g g" . consult-goto-line)
         ("M-g M-g" . consult-goto-line)
         ("M-g o" . consult-outline)
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-project-imenu)
         ("M-s f" . op/consult-find)
         ("M-s g" . consult-grep)
         ("M-s l" . consult-line)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ("M-s e" . consult-isearch))
  :custom ((register-preview-delay 0)
           (register-preview-function #'consult-register-format)
           ;; use consult to select xref locations with preview
           (xref-show-xrefs-function #'consult-xref)
           (xref-show-definitions-function #'consult-xref)
           (consult-narrow-key "<")
           (consult-project-root #'project-roots)
           (consult-find-args "find .")
           (consult-grep-args "grep --null --follow --line-buffered --ignore-case -RIn"))
  :init
  (advice-add #'register-preview :override #'consult-register-window)

  :config
  ;; make narrowing help available in the minibuffer.
  (define-key consult-narrow-map (vconcat consult-narrow-key "?")
              #'consult-narrow-help)

  ;; a find-builder that works with OpenBSD' find
  (defun op/consult--find-builder (input)
  "Build command line given INPUT."
  (pcase-let* ((cmd (split-string-and-unquote consult-find-args))
               (type (consult--find-regexp-type (car cmd)))
               (`(,arg . ,opts) (consult--command-split input))
               (`(,re . ,hl) (funcall consult--regexp-compiler arg type)))
    (when re
      (list :command
            (append cmd
                    (cdr (mapcan
                          (lambda (x)
                            `("-and" "-iname"
                              ,(format "*%s*" x)))
                          re))
                    opts)
            :highlight hl))))

  (defun op/consult-find (&optional dir)
    (interactive "P")
    (let* ((prompt-dir (consult--directory-prompt "Find" dir))
           (default-directory (cdr prompt-dir)))
      (find-file (consult--find (car prompt-dir) #'op/consult--find-builder ""))))))

;; `vertico'
(use-package vertico
  :bind
  (:map vertico-map
        ("C-j" . vertico-next)
        ("C-k" . vertico-previous)
	("M-h" . vertico-directory-up)) ;; `vertico-directory-up' shortens the target by directory separator
  ;; :blackout (foo-mode . " Foo") ; results in (blackout 'foo-mode " Foo")
  :init
  (setq vertico-scroll-margin 0) ;; Scroll margin
  ;; (setq vertico-count 10)        ;; Candidates
  (setq vertico-cycle t)         ;; Enable cycling when at top or bottom
  :config
  (vertico-mode 1)          ;; this call was placed in init instead of config section as per vertico documentation. but it seems it works in config section too and I feel it is more appropriate
  (setq completion-in-region-function #'consult-completion-in-region))

;; begin recommendation by vertico author
;; Add prompt indicator to `completing-read-multiple'.
;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
(defun crm-indicator (args)
  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))
(advice-add #'completing-read-multiple :filter-args #'crm-indicator)

;; Do not allow the cursor to move into prompt area of minibuffer
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
;; end recommendation by vertico author

;; *** `affe' : asynchronous fuzzy finder for emacs
(use-package affe
  :after orderless
  :custom ((affe-regexp-function #'orderless-pattern-compiler)
           (affe-highlight-function #'orderless-highlight-matches)))

;; *** `orderless' - a completion style using space-separated patters in any order
(use-package orderless
  :init
  (setq completion-styles '(orderless basic)) ;; basic completion style is specified as fallback in addition to orderless in order to ensure that completion commands which rely on dynamic completion tables, e.g., completion-table-dynamic or completion-table-in-turn, work correctly
  (setq completion-category-defaults nil)
  ;; enable partial-completion for file path expansion. partial-completion is important for file wildcard support. Multiple files can be opened at once with find-file if you enter a wildcard. You may also give the initials completion style a try.
  (setq completion-category-overrides '((file (styles . (partial-completion)))))
  (setq completion-styles '(orderless basic)
	completion-category-defaults nil
	completion-category-overrides '((file (styles . (partial-completion))))))

;; *** `marginalia' enable richer annotations including richer annotation of
;; completion candidates
(use-package marginalia
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  :custom (marginalia-annotators
           '(marginalia-annotators-heavy marginalia-annotators-light nil))
  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))

;; *** `embark' package for mini-buffer actions and right-click contextual menu functionality
;; Embark provides custom actions on the minibuffer (technically everywhere)
(use-package embark
  :demand t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :config
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))
(comment  ;; bindings used by omarpolo
 (use-package embark
   :bind (("C-." . embark-act)
         :map minibuffer-local-completion-map
              ("M-t" . embark-act)
              ("M-h" . embark-become)
              :map minibuffer-local-map
              ("M-t" . embark-act)
              ("M-h" . embark-become))))

(use-package embark-consult
  :after (embark consult))

;; ** `corfu' mode that generates in-buffer completions
(use-package corfu
  :custom
  (corfu-cycle t)                ;; Enable cycling for `corfu-next/previous'
  ;(corfu-auto t)                 ;; Enable auto completion
  (corfu-separator ?\s)          ;; Orderless field separator (M-SPC inserts fioed separator)
  ;; (corfu-quit-at-boundary nil)   ;; Never quit at completion boundary
  ;; (corfu-quit-no-match nil)      ;; Never quit, even if there is no match
  ;; (corfu-preview-current nil)    ;; Disable current candidate preview
  ;; (corfu-preselect 'prompt)      ;; Preselect the prompt
  ;; (corfu-on-exact-match nil)     ;; Configure handling of exact matches
  (corfu-scroll-margin 5)        ;; Use scroll margin

  ;; Enable Corfu only for certain modes.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.
  ;; This is recommended since Dabbrev can be used globally (M-/).
  ;; See also `global-corfu-modes'.
  :init
  (global-corfu-mode)
  :config
  (corfu-popupinfo-mode 1)
  (eldoc-add-command #'corfu-insert)
  :bind (("M-p" . corfu-popupinfo-scroll-down)
	 ("M-n" . corfu-popupinfo-scroll-up)
	 ("M-d" . corfu-popupinfo-toggle)))


(comment
 (look-at "shackle" package which is a rule-based manager for pop-up windows
	  https://depp.brause.cc/shackle/))

;; *** `avy'
(use-package avy
  :bind
  (("C-z" . avy-goto-char-2)
  ;; alternate bindings
  ;; ("M-g c" . avy-goto-char)
  ;; ("M-g C" . avy-goto-char-2)
  ;; ("M-g w" . avy-goto-word-1)
  ;; ("M-g f" . avy-goto-line)
   :map isearch-mode-map
   ("C-'" . avy-isearch))
  :custom
    (avy-keys '(?a ?o ?e ?u ?h ?t ?n ?s)))
