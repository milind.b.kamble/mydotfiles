;;  -*- lexical-binding: t; -*-

;;; `mbk/add-timestamp-to-messages'
;; from https://emacs.stackexchange.com/questions/32150/how-to-add-a-timestamp-to-each-entry-in-emacs-messages-buffer
(defvar mbk/last-message nil
  "Last message with timestamp appended to it.")

(defun mbk/add-timestamp-message (format-string &rest args)
  "Prepend timestamp to each message in message buffer.

FORMAT-STRING and ARGS are used by `message' to print a formatted string.

Enable with (add-hook 'find-file-hook 'mbk/add-timestamp-message)"
  (when (and message-log-max
             (not (string-equal format-string "%s%s")))
    (let ((formatted-message-string (if args
                                        (apply 'format `(,format-string ,@args))
                                      format-string)))
      (unless (string= formatted-message-string mbk/last-message)
        (setq mbk/last-message formatted-message-string)
        (let ((deactivate-mark nil)
              (inhibit-read-only t))
          (with-current-buffer "*Messages*"
            (goto-char (point-max))
            (when (not (bolp))
              (newline))
            (insert (format-time-string "[%F %T.%3N] "))))))))
(defun mbk/toggle-message-timestamp (value)
  "Toggle presence of timestamp before messages"
  (interactive "p")
  (message "arg to mbk/toggle-message-timestamp = %d flag=%s" value (> value 0))
  (if (> value 0) (advice-add 'message :before 'mbk/add-timestamp-message)
    (advice-remove 'message 'mbk/add-timestamp-message)))

;;; `mbk/active-minor-modes'
;; from https://stackoverflow.com/questions/3480173/show-keys-in-emacs-keymap-value
;; show user-friendly key-names instead of integers
(defun mbk/active-minor-modes ()
  (interactive)
  (message "active minor modes: %s" local-minor-modes))

;;; `mbk/eshell-recall-args'
;; "[\s-()>|;]" : \s- means whitespace, or any of the other chars since they usually delimit shell args
(defun mbk/eshell-recall-args ()
  (interactive)
  (let* ((my-hist (delete-dups
	 (mapcar
	  (lambda (str)
	    (string-trim (substring-no-properties str)))
	  (ring-elements eshell-history-ring))))
        (my-args (delete-dups
                  (-flatten
                   (-map (lambda (x) (s-split "[\s-()>|;]" x t)) my-hist)))))
    (insert (completing-read "insert arg: " my-args))))

;;; `mbk/wl-paste'
(defun mbk/wl-paste ()
  ;; when using Nyxt I discovered that URL of nyxt's current-buffer copied using copy-url cannot be accessed by C-y (yank). Instead it has to be accessed by running the command wl-clipboard. It is a pain to punch the key sequence `C-u M-! wl-copy'.Hence creating a command for it and binding it to C-c p
  "yank string from Wayland clipboard"
  (interactive)
  (if (equal (getenv "XDG_SESSION_TYPE") "wayland")
      (call-process "wl-paste" nil t t)
    (message "wl-paste is relevant only in Wayland window system")))
(bind-key "C-c y" #'mbk/wl-paste)

;;; `flush-blank-lines'
;; specialized the flush-lines function by providing a regexp for blank lines
(defun flush-blank-lines ()
  (interactive)
  (flush-lines "^[[:space:]]*$" nil nil t)) ;; t for INTERACTIVE parameter limits the action to transient region

;;; `guix-visit-package-homeurl-regexp-name'
(defun guix-visit-package-homeurl-regexp-name (arg)
  "search Guix packages by regexp in name field in GUIX-CURRENT-PROFILE and copy home-url of first matched package to kill ring"
  (interactive "Mpackage name regexp: ")
  (let* ((entries
	  ;; see info on GUIX-PACKAGE-INFO-GET-ENTRIES(). to change profile run GUIX-SET-CURRENT-PROFILE
	  ;; since we want to do a regexp search, we set the SEARCH-TYPE to 'regexp and setup the
	  ;; SEARCH-VALUES appropriately. We setup the datastructure in SEARCH-VALUES to limit the
	  ;; search only to the NAME field. Other options are available in info for GUIX-UI-GET-ENTRIES
	  (guix-package-info-get-entries guix-current-profile 'regexp arg '(name)))
	 (name-homeurl-alist   ; once we have the list, we create an alist of (NAME . URL).
					; If ENTRIES is nil, the alist is nil too
	  (mapcar (lambda (x) (cons (alist-get 'name x)
				    (alist-get 'home-url x))) entries))
	 (url (cdar name-homeurl-alist)))  ; URL is nil if entries is nil
    (cond (url 
	   (kill-new url) ; copy url to kill ring and transitively to clipboard (for use in nyxt)
	   (message (format
		     "%d packages matched the regexp %s in profile %s. First one copied to clipboard"
		     (length name-homeurl-alist) arg guix-current-profile)))
	  (t (message "regexp %s did not match any package name" arg)))))

;;; `mbk/describe-keymap'
(defun mbk/describe-keymap (keymap)
  "Describe a keymap using `substitute-command-keys'."
  (interactive
   (list (completing-read
          "Keymap: " (let (maps)
                       (mapatoms (lambda (sym)
                                   (and (boundp sym)
                                        (keymapp (symbol-value sym))
                                        (push sym maps))))
                       maps)
          nil t)))
  (with-output-to-temp-buffer (format "*keymap: %s*" keymap)
    (princ (format "%s\n\n" keymap))
    (princ (substitute-command-keys (format "\\{%s}" keymap)))
    (with-current-buffer standard-output ;; temp buffer
      (setq help-xref-stack-item (list #'mbk/describe-keymap keymap)))))

;;; `mbk/find-library-name'
(defun mbk/find-library-name (arg)
  "Show the full path of package or library"
  (interactive "Mpackage/library: ")
  (message (find-library-name arg)))

;;; `mbk/lookup-key'
;; from https://emacs.stackexchange.com/questions/653/how-can-i-find-out-in-which-keymap-a-key-is-bound
;; How can I find out in which keymap a key is bound?
(defun mbk/lookup-key (key)
  "Search for KEY in all known keymaps."
  (mapatoms (lambda (ob) (when (and (boundp ob) (keymapp (symbol-value ob)))
                      (when (functionp (lookup-key (symbol-value ob) key))
                        (message "%S" ob))))
            obarray))

;;; `mbk/lookup-key-prefix'
(defun mbk/lookup-key-prefix (key)
  "Search for KEY as prefix in all known keymaps."
  (mapatoms (lambda (ob) (when (and (boundp ob) (keymapp (symbol-value ob)))
                           (when (let ((m (lookup-key (symbol-value ob) key)))
                                   (and m (or (symbolp m) (keymapp m))))
                             (message "%S" ob))))
            obarray))
(defun mbk/get-keymaps ()
  (interactive)
  (mapatoms
   (lambda (x)
     (and (keymapp x)
	      (message (symbol-name x))))))

;;; `toggle-comment-slant'
;; toggle comments between slant and normal (needed for keyboardlayout editing (eg sofle))
(defun toggle-comment-slant
    (interactive)
  (let ((newslant (if (eq (face-attribute 'font-loclk-comment-face :slant) 'italic)
		      'normal 'italic)))
    (set-face-attribute 'font-loclk-comment-face nil :slant newslant)))

;;; `mbk/pp-aw-dispatch-alist'
;; string representation of aw-dispatch-alist is a list of 3-tuples
;; ((CHAR FUNC DESC) (...) ...), where the CHAR is displayed as an integer.
;; this is highly annoying. So this function prints the char representation of CHAR
(defun mbk/pp-aw-dispatch-alist ()
  "pretty print the ace-window variable aw-dispatch-alist."
  (interactive)
  (cl-prettyprint
   ;; X is a 3-element list. Return a new list with first element as CHAR print represntation and the other two elements as is
   (cl-mapcar (lambda (x)
                `(,(prin1-char (car x)) ,@(cdr x))) aw-dispatch-alist)))

;;; `mbk/bookmark-set-url'
;; Add browser bookmark to bookmark browser
;; https://emacs.stackexchange.com/questions/50433/add-browser-bookmark-to-bookmark-browser
(defun mbk/bookmark-set-url (url)
  (interactive "sBookmark URL: ")
  (if (assoc url bookmark-alist)
      (user-error "%s is already bookmarked" url)
    (push `(,url . ((handler . ,(lambda (bookmark)
                                  (browse-url (car bookmark))))))
          bookmark-alist)))

;;; `mbk/show-env-var'
;; process-environment is a list of form ENVVARNAME=VALUE
;; we map over each element and extract the string before the "="
(defun mbk/show-env-var (var)
  "show value of ENV variable in mini buffer area"
  (interactive
   (list (completing-read
          "Env var: " (--map (car (s-split "=" it)) process-environment))))
  (message (pp (getenv var))))

;;; `chagnge-bracket-pairs'
(defun xah-change-bracket-pairs ( FromChars ToChars)
  "Change bracket pairs to another type or none.
For example, change all parenthesis () to square brackets [].
Works on current block or selection.

When called in lisp program, FromChars or ToChars is a string of bracket pair. eg \"(paren)\",  \"[bracket]\", etc.
The first and last characters are used. (the middle is for convenience in ido selection.)
If the string contains “,2”, then the first 2 chars and last 2 chars are used, for example  \"[[bracket,2]]\".
If ToChars is equal to string “none”, the brackets are deleted.

URL `http://xahlee.info/emacs/emacs/elisp_change_brackets.html'
Version: 2020-11-01 2021-08-15 2022-04-07"
  (interactive
   (let (($brackets
          '("(paren)"
            "{brace}"
            "[square]"
            "<greater>"
            "`emacs'"
            "`markdown`"
            "~tilde~"
            "=equal="
            "\"double\""
            "'single'"
            "[[double square,2]]"
            "“curly double”"
            "‘curly single’"
            "‹french angle›"
            "«french double angle»"
            "「corner」"
            "『white corner』"
            "【lenticular】"
            "〖white lenticular〗"
            "〈angle〉"
            "《double angle》"
            "〔tortoise〕"
            "〘white tortoise〙"
            "⦅white paren⦆"
            "〚white square〛"
            "⦃white curly⦄"
            "〈pointing angle〉"
            "⦑ANGLE WITH DOT⦒"
            "⧼CURVED ANGLE⧽"
            "⟦math square⟧"
            "⟨math angle⟩"
            "⟪math DOUBLE ANGLE⟫"
            "⟮math FLATTENED PARENTHESIS⟯"
            "⟬math WHITE TORTOISE SHELL⟭"
            "❛HEAVY SINGLE QUOTATION MARK ORNAMENT❜"
            "❝HEAVY DOUBLE TURNED COMMA QUOTATION MARK ORNAMENT❞"
            "❨MEDIUM LEFT PARENTHESIS ORNAMENT❩"
            "❪MEDIUM FLATTENED LEFT PARENTHESIS ORNAMENT❫"
            "❴MEDIUM LEFT CURLY ORNAMENT❵"
            "❬MEDIUM LEFT-POINTING ANGLE ORNAMENT❭"
            "❮HEAVY LEFT-POINTING ANGLE QUOTATION MARK ORNAMENT❯"
            "❰HEAVY LEFT-POINTING ANGLE ORNAMENT❱"
            "none"
            )))
     (list
      (completing-read "Replace this:" $brackets )
      (completing-read "To:" $brackets ))))
  (let ( $p1 $p2 )
    (let (($bds (xah-get-bounds-of-block-or-region))) (setq $p1 (car $bds) $p2 (cdr $bds)))
    (save-excursion
      (save-restriction
        (narrow-to-region $p1 $p2)
        (let ( (case-fold-search nil) $fromLeft $fromRight $toLeft $toRight)
          (cond
           ((string-match ",2" FromChars  )
            (progn
              (setq $fromLeft (substring FromChars 0 2))
              (setq $fromRight (substring FromChars -2))))
           (t
            (progn
              (setq $fromLeft (substring FromChars 0 1))
              (setq $fromRight (substring FromChars -1)))))
          (cond
           ((string-match ",2" ToChars)
            (progn
              (setq $toLeft (substring ToChars 0 2))
              (setq $toRight (substring ToChars -2))))
           ((string-match "none" ToChars)
            (progn
              (setq $toLeft "")
              (setq $toRight "")))
           (t
            (progn
              (setq $toLeft (substring ToChars 0 1))
              (setq $toRight (substring ToChars -1)))))
          (cond
           ((string-match "markdown" FromChars)
            (progn
              (goto-char (point-min))
              (while
                  (re-search-forward "`\\([^`]+?\\)`" nil t)
                (overlay-put (make-overlay (match-beginning 0) (match-end 0)) 'face 'highlight)
                (replace-match (concat $toLeft "\\1" $toRight ) t ))))
           ((string-match "tilde" FromChars)
            (progn
              (goto-char (point-min))
              (while
                  (re-search-forward "~\\([^~]+?\\)~" nil t)
                (overlay-put (make-overlay (match-beginning 0) (match-end 0)) 'face 'highlight)
                (replace-match (concat $toLeft "\\1" $toRight ) t ))))
           ((string-match "ascii quote" FromChars)
            (progn
              (goto-char (point-min))
              (while
                  (re-search-forward "\"\\([^\"]+?\\)\"" nil t)
                (overlay-put (make-overlay (match-beginning 0) (match-end 0)) 'face 'highlight)
                (replace-match (concat $toLeft "\\1" $toRight ) t ))))
           ((string-match "equal" FromChars)
            (progn
              (goto-char (point-min))
              (while
                  (re-search-forward "=\\([^=]+?\\)=" nil t)
                (overlay-put (make-overlay (match-beginning 0) (match-end 0)) 'face 'highlight)
                (replace-match (concat $toLeft "\\1" $toRight ) t ))))
           (t (progn
                (progn
                  (goto-char (point-min))
                  (while (search-forward $fromLeft nil t)
                    (overlay-put (make-overlay (match-beginning 0) (match-end 0)) 'face 'highlight)
                    (replace-match $toLeft t t)))
                (progn
                  (goto-char (point-min))
                  (while (search-forward $fromRight nil t)
                    (overlay-put (make-overlay (match-beginning 0) (match-end 0)) 'face 'highlight)
                    (replace-match $toRight t t)))))))))))

;;; `mbk/display-ansi-color'
;; from https://stackoverflow.com/questions/23378271/how-do-i-display-ansi-color-codes-in-emacs-for-any-mode
;; use this when viewing a file in a buffer that contains ansi color code raw escape codes
(require 'ansi-color)
(defun mbk/display-ansi-colors ()
  "show ansi colors when a buffer visits a file conatianing ansi color codes"
  (interactive)
  (ansi-color-apply-on-region (point-min) (point-max)))

;;; `mbk/elpy-nav-forward-block'
;; from ~/.guix-home/profile/share/emacs/site-lisp/elpy-*/elpy.el
(defun mbk/elpy-nav-forward-block ()
  "Move to the next line indented like point.

This will skip over lines and statements with different
indentation levels."
  (interactive "^")
  (let ((indent (current-column))
        (start (point))
        (cur nil))
    (when (/= (% indent python-indent-offset)
              0)
      (setq indent (* (1+ (/ indent python-indent-offset))
                      python-indent-offset)))
    (python-nav-forward-statement)
    (while (and (< indent (current-indentation))
                (not (eobp)))
      (when (equal (point) cur)
        (error "Statement does not finish"))
      (setq cur (point))
      (python-nav-forward-statement))
    (when (< (current-indentation)
             indent)
      (goto-char start))))

;;; `mbk/elpy-nav-forward-block'
(defun mbk/elpy-nav-backward-block ()
  "Move to the previous line indented like point.

This will skip over lines and statements with different
indentation levels."
  (interactive "^")
  (let ((indent (current-column))
        (start (point))
        (cur nil))
    (when (/= (% indent python-indent-offset)
              0)
      (setq indent (* (1+ (/ indent python-indent-offset))
                      python-indent-offset)))
    (python-nav-backward-statement)
    (while (and (< indent (current-indentation))
                (not (bobp)))
      (when (equal (point) cur)
        (error "Statement does not start"))
      (setq cur (point))
      (python-nav-backward-statement))
    (when (< (current-indentation)
             indent)
      (goto-char start))))

;;; `mbk/buffer-active-minor-modes'
;; to find list of active minor modes in a buffer: (https://stackoverflow.com/questions/1511737/how-do-you-list-the-active-minor-modes-in-emacs)
(defun mbk/buffer-active-minor-modes (buff)
  "Show active minor modes for given BUFFER. BUFFER is read from user input"
  (interactive "b")
  (with-current-buffer buff
    (--filter (and (boundp it) (symbol-value it)) minor-mode-list)))
(defun mbk/current-buffer-minor-mode-activep (minor-mode)
  "Is given MINOR-MODE active in current buffer"
  (interactive
   (list (completing-read "Minor mode: "
                          (mbk/buffer-active-minor-modes (current-buffer)))))
  ;; completing-read already was given only active mior modes. So whatever user chooses is enabled.
  (message "%s : enabled" minor-mode))

;;; `mbk/piavpn-get-servers' using tree prober of the datastructure generated by json-parse
;; the following three query-tree* functions are borrowed from https://howardism.org/Technical/Emacs/query-data-structures.html
;; they are elisp quuivalent of `jq' command line tool for making json queries
;; the assumed data structure of the tree is a hash table or array or "scalar" as elements of a node in a tree. this is the data structure returned by `json-parse-string' and `json-parse-buffer'
;; 
(defun query-tree (data-structure &rest query)
  "Return value from DATA-STRUCTURE based on elements of QUERY."
  (query-tree-parse query data-structure))
(defun query-tree-parse (query tree)
  "Return value of QUERY from TREE data structure."
  (if (null query)
      tree
    (query-tree-parse (cdr query)
                      (query-tree--parse-node (car query) tree))))
(defun query-tree--parse-node (node tree)
  "Return results of TREE data structure of symbol, NODE.

If TREE is a hash-table, then NODE would be the key, either a
string or a symbol. Return value.

If TREE is a sequence and NODE is number, Return element indexed
by NODE.

If TREE is a Property List, return value of NODE as a key. This
should work with association lists as well."
  (cond
   ((null tree)                        nil)
   ((hash-table-p tree)                 (or (gethash node tree)
                                            (gethash (symbol-name node) tree)))
   ((and (numberp node)  (seqp tree))       (seq-elt tree node))
   ((and (eq node :first) (seqp tree))      (car node))
   ((and (eq node :last) (seqp tree))       (last node))
   ((plistp tree)                           (plist-get tree node))
   ((assoc node tree)                       (cdr (assoc node tree)))
   (t                                  (error "query-tree: Looking for '%s' in %s" node tree))))

(defun mbk/piavpn-get-servers ()
  (require 'url)
  (require 'json)
  (with-temp-buffer
    (url-insert-file-contents "https://serverlist.piaservers.net/vpninfo/servers/v6")
    (goto-char (point-min))
    (json-parse-buffer)))
;; usage: (setq foo (mbk/piavpn-get-servers)) (seq-map (lambda (x) (query-tree x 'servers 'meta 0 'ip)) (query-tree foo 'regions))


;; see also https://github.com/phillord/pabbrev/blob/master/pabbrev.el

;;; `beancount-accounts' -- obsolete because beancount-mode already has support for this
;; https://whatacold.io/blog/2022-09-10-emacs-beancount-account-files/ (Auto-complete Accounts From Other Beancount Files)
;; here is how to also auto-complete accounts from other files using the advice mechanism of Emacs
(comment (defvar beancount-account-files nil
           "List of account files")

         (defun w/beancount--collect-accounts-from-files (oldfun regex n)
           (let ((keys (funcall oldfun regex n))
                 (hash (make-hash-table :test 'equal)))
             (dolist (key keys)
               (puthash key nil hash))
             ;; collect accounts from files
             (save-excursion
               (dolist (f beancount-account-files)
                 (with-current-buffer (find-file-noselect f)
                   (goto-char (point-min))
                   (while (re-search-forward beancount-account-regexp nil t)
                     (puthash (match-string-no-properties n) nil hash)))))
             (hash-table-keys hash))))
;; (advice-add #'beancount-collect
;;             :around #'w/beancount--collect-accounts-from-files
;;             '((name . "collect accounts from files as well")))

;;; `unlock-keepass' unlocks KeePass app using DBus
;; read file content into a string http://ergoemacs.org/emacs/elisp_read_file_content.html
(defun read-file-to-string (filePath)
  "Return filePath's file content."
  (with-temp-buffer
    (insert-file-contents filePath)
    (buffer-string)))
(defun read-lines (filepath)
  (interactive)
  (split-string (read-file-to-string filepath) "\n" t))

;; *** unlock keepassxc DB from emacs using dbus
;; Unlock the KeepassXC GUI-app
;; remotely without explicitly entering the password
;; Lot of learning and experimenting during development, but the
;; shortness of code is impressive.
;; Reading the safebox.gpg file auto decrypts it into plain text
;; (thanks to EPA). parson-parse converts json-structured
;; string into hierarchical alist, from which we extract the values of
;; password and security file. Finally the dbus API is used to execute
;; the method openDatabase on the keepass service
(defun unlock-keepass ()
  "unlock keepass app using dbus"
  (interactive)
  (require 'dbus)
  (let* ((strong-box (expand-file-name
		      "~/.gnupg/keepass/keepass_safebox.gpg"))
	 (kdbx (expand-file-name
		"~/.gnupg/keepass/khaazgee_milind_keepass.kdbx"))
	 (pwd-db (with-temp-buffer
		   (insert-file-contents strong-box)
		   (json-parse-buffer)))
	 (kdbx-sec (gethash "keepasssec" (gethash "mil" pwd-db)))
	 (pwd (gethash "keepasspwd" (gethash "mil" pwd-db))))
    (cl-assert pwd-db nil "Could not parse strong-box") ;; pwd-db should be non-nil
    (cl-assert pwd nil "Password was not found in strong-box")
    (cl-assert kdbx-sec nil "Keepass security filepath not found in strong-box")
    (dbus-call-method :session "org.keepassxc.KeePassXC.MainWindow"
		      "/keepassxc" "org.keepassxc.KeePassXC.MainWindow" "openDatabase"
		      kdbx pwd kdbx-sec)))

;;; `mbk/set-selective-display-glyph' experimental code
(defun mbk/set-selective-display-glyph ()
  (interactive)
  (let ((display-table (or buffer-display-table (make-display-table))))
    (set-display-table-slot display-table 'selective-display
                            (vconcat (mapcar (lambda (c) (make-glyph-code c 'font-lock-comment-face)) mbk-ellipsis)))
    (setq buffer-display-table display-table)))

;;; `mbk/ancestor-modes-of'
;; show hierarhy of modes that given mode is derived from
;; https://emacs.stackexchange.com/questions/58073/how-to-find-inheritance-of-modes
(defun mbk/ancestor-modes-of (buffer)
  "Return a list of the ancestor modes that BUFFER major mode is derived from."
  (interactive "b")
  (let ((modes   ())
        (mode (with-current-buffer buffer major-mode))
        (parent  nil))
    (while (setq parent (get mode 'derived-mode-parent))
      (push parent modes)
      (setq mode parent))
    ;; (setq modes  (nreverse modes))
    (message "%s "(nreverse modes))
    ))


;; no need. we will load using (load "utils")
;; (provide 'utils)

