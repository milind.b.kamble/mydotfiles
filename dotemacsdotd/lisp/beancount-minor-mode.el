(require 'beancount)
(defun bc-backward-transaction (&optional arg)
  "Move to prevtransaction. With an argument move that many transactions"
  (interactive "p")
  (beancount-goto-transaction-begin)
  (re-search-backward beancount-transaction-regexp nil t arg)
  (beancount-goto-transaction-begin))
(defun bc-forward-transaction (&optional arg)
  "Move to next transaction. With an argument move that many transactions"
  (interactive "p")
  (beancount-goto-transaction-end)
  (re-search-forward beancount-transaction-regexp nil t arg)
  (beancount-goto-transaction-begin))

;;;###autoload
(defun bc-enable-if-filename-match ()
  (when (string-match "beancount\\(.asc\\)?\\'\\|bct\\'" (buffer-file-name))
      (message "ran bc-enable-if-filename-match")
      (beancount-minor-mode 1)
      (variable-pitch-mode -1))
    )

(defvar beancount-minor-mode-map (make-sparse-keymap)
  "Keymap for beancount minor mode")

;;;###autoload
(define-minor-mode beancount-minor-mode
  "A minor mode for beancount files"
  :lighter " bc"
  :keymap beancount-minor-mode-map
  ;; :keymap (let ((map (make-sparse-keymap)))
  ;;           (define-key map (kbd "C-c n") 'bc-forward-transaction)
  ;; 	    (define-key map (kbd "C-c p") 'bc-backward-transaction)
  ;; 	    map)
  )
(provide 'beancount-minor-mode)
