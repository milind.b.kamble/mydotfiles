;;; `dabbrev'
;; By default M-/ is dabbrev-expand, C-M-/ is dabbrev-complete
;; Swap M-/ and C-M-/
;; M-/ is quicker to type than C-M-/ and we prefer completion rater than expansion
;; we remap dabbrev-expand to hippie-expand
;; hippie-expand is similar to dabbrev-completion in that it cycles through candidates, but
;; dabbrev-completion is more visually interactive
(bind-keys ("M-/" . dabbrev-completion)
           ("C-M-/" . dabbrev-expand))
(global-set-key [remap dabbrev-expand] 'hippie-expand)  ;; from https://www.masteringemacs.org/article/text-expansion-hippie-expand

;;; leader key (M-SPC) keymap
(setq mbk/leader-map (make-sparse-keymap))

;; create a sparse keymap named mbk/buffer-map for buffer actions
;; bind it to "b" in mbk/leader-map
;; setup bindings in mbk/buffer-map
(bind-keys :map mbk/leader-map
	   :prefix-docstring "Keymap and bindings for buffer actions"
           :prefix-map mbk/buffer-map
	   :prefix "b" ; buffer related actions
	   ("r" . revert-buffer)
	   ("C" . display-ansi-colors)  ; convert raw escape codes into ansi color display over REGION
	   ("p" . recenter-top-bottom)  ; position the point-line at center, top or bottom
	   ("s" . ispell-buffer)        ; do a spell check on entire buffer
	   ("f" . consult-focus-lines)
	   ("s" . switch-to-buffer-other-window)
	   ("o" . consult-occur))

(bind-keys :map mbk/leader-map
	   :prefix-docstring "Generic Keymap and bindings"
	   :prefix-map mbk/generic-map
	   :prefix "g"  ; global/generic stuff
           ("k" . unlock-keepass)  ;; not needed. instead use "M-g u"
	   ("g" . el-get-describe)
	   ("e" . project-eshell))

(bind-keys :map mbk/leader-map
	   :prefix-docstring "Keymap and bindings during active editing"
	   :prefix "e" ; active editing
	   :prefix-map mbk/edit-map
	   ("l" . downcase-word)
	   (";" . comment-or-uncomment-region)
	   ("V" . calc-grab-sum-down)
	   ("H" . calc-grab-sum-across)
	   ("r" . flyspell-check-previous-highlighted-word))

(bind-keys :map mbk/leader-map
	   :prefix "m"     ; cursor motion
	   :prefix-map mbk/motion-map
	   :prefix-docstring "Keymap and bindings for cursor motion"
           (";" . consult-goto-line)
	   :repeat-map mbk/repeat-motion-map
	   ("w" . move-to-window-line-top-bottom)
           
	   )

;;; we dont need this now after switching to outli-mode `outline-minor-mode'
;; ;; see description of outline-minor-mode on EmacsWiki at https://www.emacswiki.org/emacs/OutlineMinorMode
;; ;; `hide-sublevels' hides everything except top-level-headings. `show-all' expands everything
;; ;; `hide-body' hides all bodies, only headings (all levels) are shown.
;; ;; `hide-other' hides other branches
;; ;; `show-children' this entry's immediate children entries (more deeper with prefix arg)
;; ;; `hide-entry' hide this entry's body, `show-entry' show this entry's body
;; ;; `hide-leaves' hide body lines in this entry and sub-entries - ie. show only headings under this entry, `show-branches' show all sub-headings under this heading but not their bodies
;; ;; `hide-subtree' hide everything in this entry and sub-trees (ie fully hide this node), `show-subtree' show everything under this entry
;; (add-hook 'outline-minor-mode-hook
;;           (lambda () (local-set-key "\C-c\C-c"
;;                                     outline-mode-prefix-map)))
;; (eval-after-load 'outline
;;   ;; tick is needed in fornt of progn because from has to be presented without being evaluated
;;   '(progn
;;     (message "setting up outline keymap in leader map")
;;     (defvar-keymap mbk/outline-map
;;       :doc "keymap for outline navigation, hide/show and promote/demote"
;;       ;; do not use 'consult-outline as in :repeat (:exit ('consult-outline)). it does not work
;;       ;; see https://karthinks.com/software/it-bears-repeating/
;;       :repeat (:exit (consult-outline))
;;       "u" #'outline-up-heading
;;       "g" #'consult-outline
;;       "f" #'outline-forward-same-level        "b" #'outline-backward-same-level
;;       "p" #'outline-previous-visible-heading  "n" #'outline-next-visible-heading
;;       "t" #'outline-hide-sublevels            "a" #'outline-show-all                   ;; global
;;       "h" #'outline-hide-body                                                          ;; global
;;       "r" #'hide-other                        "s" #'outline-show-children              ;; node level
;;       "d" #'outline-hide-entry                "e" #'outline-show-entry                 ;; node level
;;       "l" #'outline-hide-leaves               "m" #'outline-show-branches              ;; node level
;;       "c" #'outline-hide-subtree              "o" #'outline-show-subtree               ;; node level
;;       "v" #'outline-move-subtree-down         "w" #'outline-move-subtree-up
;;       "<" #'outline-promote                   ">" #'outline-demote) 
;;     (define-key mbk/leader-map "o" mbk/outline-map)))

;; no need (provide 'customkeybindings)  - using (load FILE) instead

(defun mbk/narrow-or-widen-dwim (p)
  "Widen if the buffer is narrowed, narrow-dwim otherwise.
Dwim means: region, org-src-block, org-subtree or defun,
whichever applies first.  Narrowing to org-src-blocks actually
calls `org-edit-src-code'.

With prefix P, don't widen, just narrow even if buffer is already
narrowed.  With P being -, narrow to page instead of to defun.

Taken from endless parentheses."
  (interactive "P")
  (declare (interactive-only))
  (cond ((and (buffer-narrowed-p) (not p)) (widen))
        ((region-active-p)
         (narrow-to-region (region-beginning)
                           (region-end)))
        ((derived-mode-p 'org-mode)
         ;; `org-edit-src-code' isn't a real narrowing
         (cond ((ignore-errors (org-edit-src-code) t))
               ((ignore-errors (org-narrow-to-block) t))
               (t (org-narrow-to-subtree))))
        ((eql p '-) (narrow-to-page))
        (t (narrow-to-defun))))
(define-key global-map (kbd "C-c w") #'mbk/narrow-or-widen-dwim)
