(setq
 completion-styles '(orderless basic) ;; basic completion style is specified as fallback in addition to orderless in order to ensure that completion commands which rely on dynamic completion tables, e.g., completion-table-dynamic or completion-table-in-turn, work correctly
  completion-category-defaults nil

  ;; enable partial-completion for file path expansion. partial-completion is important for file wildcard support. Multiple files can be opened at once with find-file if you enter a wildcard. You may also give the initials completion style a try.
  completion-category-overrides '((file (styles . (partial-completion)))))
