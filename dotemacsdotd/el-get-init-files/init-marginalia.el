(with-eval-after-load 'marginalia
  (bind-keys
   ("M-A" . marginalia-cycle)
   :map minibuffer-local-map
   ("M-A" . marginalia-cycle)
   )
  (setopt marginalia-annotators
          '(marginalia-annotators-heavy marginalia-annotators-light nil))
  )
(marginalia-mode)
