(with-eval-after-load 'vertico
  (bind-keys :map vertico-map
	   ("C-j" . vertico-next)
	    ("C-k" . vertico-previous)
	    ("M-h" . vertico-directory-up)))

(setq vertico-scroll-margin 0 ;; Scroll margin
      ;; vertico-count 10        ;; Candidates
      vertico-cycle t         ;; Enable cycling when at top or bottom
      completion-in-region-function #'consult-completion-in-region)

(vertico-mode 1) ;; this call shoulde be done before package load or after?

;; begin recommendation by vertico author
;; Add prompt indicator to `completing-read-multiple'.
;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
(defun crm-indicator (args)
  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))
(advice-add #'completing-read-multiple :filter-args #'crm-indicator)

;; Do not allow the cursor to move into prompt area of minibuffer
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
;; end recommendation by vertico author
