(message "loading init-ace-window.el")
(setopt aw-keys '(?a ?o ?e ?u ?h) ;; we shouldn't be having more than 5 windows in a frame
        ;; aw-swap-invert moves point to target window, thus keeping point in the orig buffer
        aw-swap-invert t
        aw-dispatch-always t ;; aw will perform read-char even when there is single window. allows us to split windows, switch buffers etc.
        aw-dispatch-alist
        '((?k aw-delete-window "Delete Window")
          (?s aw-swap-window "Swap Window")
          (?b aw-switch-buffer-in-window "Switch Buffer")
          (?r aw-switch-buffer-other-window "Switch Buffer Other Window")
          (?m aw-move-window "Move Window")
          (?i aw-execute-command-other-window "Exec Command Other Window")          
          (?p aw-flip-window)
          (?- aw-split-window-vert "Split Vert Window")
          (?: aw-split-window-horz "Split Horz Window")
          (?x delete-other-windows "Maximize Window")
          (?T aw-transpose-frame "Transpose Frame")  ;; will work when transpose-frame package is installed
          (?? aw-show-dispatch-help)))
(bind-keys ("M-o" . ace-window) ;; bind in global-map
           ;; ("M-O" . switch-to-buffer-other-window) we don't need this
           )
