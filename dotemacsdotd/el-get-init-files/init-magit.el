(message "loading init-magit.el")
(global-set-key (kbd "C-x g") 'magit-status)
(setopt magit-diff-refine-hunk t
        transient-default-level 5)

;; 
(comment (bind-keys :map magit-mode-map
           ("C-o" . mbk/magit-dired-other-window))
(defun mbk/magit-dired-other-window ()
  (interactive)
  (dired-other-window (magit-toplevel))))
