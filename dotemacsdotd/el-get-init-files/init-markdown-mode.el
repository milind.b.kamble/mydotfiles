(add-to-list 'auto-mode-alist
             '("README\\.md\\'" . gfm-mode))
(add-to-list 'auto-mode-alist
             '("\\.md\\'" . markdown-mode))
(add-to-list 'auto-mode-alist
             '("\\.markdown\\'" . markdown-mode))
(setopt markdown-command "cl-markdown") ;; TBD
(add-hook 'markdown-mode-hook #'variable-pitch-mode)
