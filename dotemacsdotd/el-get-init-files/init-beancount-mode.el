;; this file name has to match the pkg name used in the el-get-bundle! call
;; (message "mbkloading init-beancount.el")

(setopt beancount-use-ido nil)
(bind-keys :map beancount-mode-map
 ("C-c n" . beancount-goto-next-transaction)
 ("C-c p" . beancount-goto-previous-transaction)
 :repeat-map mbk/beancount-trans-repeat-map
 ("n" . beancount-goto-next-transaction)
 ("p" . beancount-goto-previous-transaction)
 )
(defun bc-simple-dt (dt)
    "Read a string of form [[YEAR] MM] DD and convert it to beancount compliant date. single digits are appropriately padded. Default value for YEAR is file-variable `bc-year' or 2000 and for MM is 01"
    (interactive "M")
    ;; (message "dt=%s" dt)
  (defun mth-dt-pad0 (mth-or-dt)
    (substring (concat "00" mth-or-dt) -2 nil))
  (defun yr-pad (yr)
    (concat "20" (mth-dt-pad0 yr)))
  (pcase (split-string dt)
    (`(,y ,m ,d) (format "%s-%s-%s" (yr-pad y) (mth-dt-pad0 m) (mth-dt-pad0 d)))
    (`(,m ,d) (format "%s-%s-%s" (if (boundp 'bc-year) bc-year "2000") (mth-dt-pad0 m) (mth-dt-pad0 d)))
    (`(,d) (format "%s-%s-%s" (if (boundp 'bc-year) bc-year "2000") (mth-dt-pad0 d)))
    (_ (error "unsupported input form"))))
