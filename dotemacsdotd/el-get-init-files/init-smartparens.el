(message "loading init-smartparens.el")

(sp-use-smartparens-bindings)
;; configuration created using https://www.omarpolo.com/dots/emacs.html for reference
(bind-keys :map smartparens-mode-map
           ("C-k" . sp-kill-hybrid-sexp)
	   ("C-," . sp-rewrap-sexp)
           :map lisp-data-mode-map (";" . sp-comment))
(bind-key [remap c-electric-backspace] #'sp-backward-delete-char
          smartparens-strict-mode-map)
(setq sp-show-pair-from-inside nil) ;;

(smartparens-global-mode t)
(sp-with-modes 'org-mode
               (sp-local-pair "=" "=" :wrap "C-="))

(add-hook 'prog-mode-hook #'turn-on-smartparens-strict-mode)
;; wait till web-mode is instantiated (add-hook 'web-mode-hook (lambda () (setq web-mode-enable-auto-pairing nil))
(add-hook 'tex-mode-hook #'turn-on-smartparens-strict-mode)

;; Notes
;; there is a command: sp-cheat-sheet that generates a buffer with description of each SP command with examples
;; see also https://gist.github.com/pvik/8eb5755cc34da0226e3fc23a320a3c95 a gist with animated examples)
;; DEL : sp-backward-delete-char. Deleted char backward or moves point inside over a delimiter.
;;                                  with numeric prefix of 0, delete char backward without regard for balancing
;;                                  with C-u, delete backward until an opening delimiter.whose deletion would break balancing is hit
;; C-d  : sp-delete-char           complement to sp-backward-delete-char
;; M-DEL : sp-unwrap-sexp             unwrap following sexp. With -ve arg, unwrap backwards. C-u works like prefix +4. with arg N, looks forward for Nth closing pair and unwraps that sexp
;;       sp-backward-unwrap-sexp is just a wrapper around sp-unwrap-sexp
;; C-, : sp-rewrap-sexp            rewrap the eclosing expr with a different pair. When new pair is empty string (hitting RET in minibuffer), it deletes the old pair. This is a way to remove delimiters. Numeric arg does not work. So better to use M-DEL for unwrapping
;;                                 with C-u, keeps the old pair
;; C-k : sp-kill-hybrid-sexp       kills line from point onwards but in such a way that result remains balanced
;;                                 with C-u C-u kill the hybrid sexp point is in
;;                                 with 0 prefix, normal kill-line
;; C-S-Backspace : sp-kill-whole-line  kill current line in sexp-aware manner. Go to beg of current line and then try to kill as much as possible.
;; C-w : sp-kill-region            kill region in sexp-aware manner. With C-u skip balance check

;; C-BKSPC : sp-backward-kill-word    kill one/N word backward skipping over delimiters
;; M-d : sp-kill-word              complement of sp-backward-kill-word

;; C-LEFT : sp-forward-barf-sexp      move right paren towards left by one/N sexp
;; C-RIGHT : sp-forward-slurp-sexp    move right paren towards right by one/N sexp
;; C-M-BKSPC : sp-splice-sexp-killing-backward
;; C-M-DEL : sp-splice-sexp-killing-forward
;; C-M-LEFT : sp-backward-slurp-sexp  move left paren towards left by one/N sexp
;; C-M-RIGHT : sp-backward-barf-sexp  move left paren towards right by one/N sexp
;; C-M-SPC   : sp-mark-sexp 
;; C-M-] : sp-select-next-thing     similar to expand-region, but moves forward to next thing
;; C-M-a : sp-backward-down-sexp
;; C-M-b : sp-backward-sexp
;; C-M-d : sp-down-sexp
;; C-M-e : sp-up-sexp
;; C-M-f : sp-forward-sexp
;; C-M-k : sp-kill-sexp
;; C-M-n : sp-next-sexp
;; C-M--p : sp-prev-sexp
;; C-M-u : sp-backward-up-sexp
;; C-M-w : sp-copy-sexp
;; C-S-BKSPC : sp-splice-sexp-killing-around
;; C-S-a : sp-end-of-sexp
;; C-S-d : sp-beginning-of-sexp
;; C-] : sp-select-next-thing-exchange
;; C-k : sp-kill-hybrid-sexp
;; M-DEL : sp-unwrap-sexp
;; M-B : sp-backward-symbol
;; M-D : sp-splice-sexp
;; M-F : sp-forward-symbol

