(message "mbk: loading init-org-mode.el")
(setopt
 ;;tbd org-agenda-files "~/notes/agenda.org"
 ;;tbd org-directory "~/docs/notes"
 org-startup-indented t
 org-adapt-indentation nil
 org-edit-src-content-indentation 0
 org-outline-path-complete-in-steps nil
 ;;tbd org-refile-use-outline-path 'full-file-path
 ;;tbd org-refile-allow-creating-parent-nodes 'confirm
 ;;tbd org-refile-targets `((nil . (:maxlevel . 3)
 ;; 			  (org-agenda-files . (:maxlevel . 3))))
 org-ellipsis "⤵"   ;; alternate: " ▾"
 org-hide-emphasis-markers t
 org-log-into-drawer t
 ;;tbd org-directory org-directory
 ;;tbd org-default-notes-file (concat org-directory "/todo.org")
 ;;tbd org-src-window-setup 'current-window
 ;;tbd org-agenda-window-setup 'current-window

 )
(org-babel-do-load-languages
 'org-babel-load-languages  ;; many more exist
 '((python . t)
   (emacs-lisp . t)
   (shell . t)
   (lisp . t)
   (gnuplot . t)
   (R . t)
   (C . t)))
(add-hook 'org-mode-hook #'variable-pitch-mode)
(add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
