;;-*-coding: utf-8;-*-
(define-abbrev-table 'beancount-mode-abbrev-table
  '(
    ("amain" "Expenses:Auto:Maint" nil :count 2)
    ("autgas" "Expenses:Auto:Gas" nil :count 0)
    ("cash" "Assets:Current:Cash" nil :count 13)
    ("cscocc" "Liabilities:CreditCard:CostcoCC" nil :count 1)
    ("emisc" "Expenses:Misc" nil :count 3)
    ("expclo" "Expenses:Clothes" nil :count 0)
    ("expdin" "Expenses:Dining" nil :count 0)
    ("expsubs" "Expenses:Subscriptions" nil :count 1)
    ("fees" "Expenses:Fees" nil :count 1)
    ("fidcc" "Liabilities:CreditCard:FidelityCC" nil :count 1)
    ("groc" "Expenses:Grocery" nil :count 2)
    ("himp" "Expenses:HomeImprov" nil :count 1)
    ("hmain" "Expenses:HomeMaint" nil :count 2)
    ("scott" "Assets:Investments:Scottrade" nil :count 3)
    ("utigas" "Expenses:Utilities:Gas" nil :count 0)
    ("venmom" "Assets:Current:VenmoMihir" nil :count 0)
    ("venmon" "Assets:Current:VenmoNeha" nil :count 0)
   ))

