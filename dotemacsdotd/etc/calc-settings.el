;; multiply TOS by a scaling factor, which in this case is the price inclusive of sales tax
(defconst var-atxSalesTax '(float 10825 -4))

;; workflow:
;; if value is not in buffer, then 'C-x * c', push value to TOS, then 'z t', then 'q' to return back to buffer, then 'C-x * y' to yank value from TOS
;; if value is the sum of numbers in a column, then mark rectangle, then 'C-x * :' which does calc-grab-sum-down, hen 'z t', then 'q', then 'C-x * y'
(put 'calc-define 'mbkamble-calc-funs
     '(progn
	;; defmath creates two functions: calc-atx-price-incl-salestax and calcFunc-atx-price-incl-salestax
	(defmath atx-price-incl-salestax (pr)
	  "compute the net price after adding sales tax (Austin or Travis County)."
	  (interactive 1 "stax")
          ;; structure of pr is either (float num exponent) or (vec (float num exponent) ...). we extract only the first element
	  (and (eq 'vec (car pr))
	       (setq pr (cadr pr))) ;; overwrite pr
          ;; atx_sales_tax converts to '(calcFun-subscr var-atx var_sales) etc.
	  ;; atx-salesl-tax converts to '(math-sub var-atx var-sales ) etc.
	  :" pr * atxSalesTax")
	(define-key calc-mode-map "zt" 'calc-atx-price-incl-salestax)))
