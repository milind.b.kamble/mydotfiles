;; -*- lexical-binding: t; -*-

;;; Initialize the pacakge manager `elpaca'
(defvar elpaca-installer-version 0.7)
(defvar elpaca-directory (expand-file-name "elpaca/" user-emacs-directory))
(defvar elpaca-builds-directory (expand-file-name "builds/" elpaca-directory))
(defvar elpaca-repos-directory (expand-file-name "repos/" elpaca-directory))
(defvar elpaca-order '(elpaca :repo "https://github.com/progfolio/elpaca.git"
                              :ref nil :depth 1
                              :files (:defaults "elpaca-test.el" (:exclude "extensions"))
                              :build (:not elpaca--activate-package)))
(let* ((repo  (expand-file-name "elpaca/" elpaca-repos-directory))
       (build (expand-file-name "elpaca/" elpaca-builds-directory))
       (order (cdr elpaca-order))
       (default-directory repo))
  (add-to-list 'load-path (if (file-exists-p build) build repo))
  (unless (file-exists-p repo)
    (make-directory repo t)
    (when (< emacs-major-version 28) (require 'subr-x))
    (condition-case-unless-debug err
        (if-let ((buffer (pop-to-buffer-same-window "*elpaca-bootstrap*"))
                 ((zerop (apply #'call-process `("git" nil ,buffer t "clone"
                                                 ,@(when-let ((depth (plist-get order :depth)))
                                                     (list (format "--depth=%d" depth) "--no-single-branch"))
                                                 ,(plist-get order :repo) ,repo))))
                 ((zerop (call-process "git" nil buffer t "checkout"
                                       (or (plist-get order :ref) "--"))))
                 (emacs (concat invocation-directory invocation-name))
                 ((zerop (call-process emacs nil buffer nil "-Q" "-L" "." "--batch"
                                       "--eval" "(byte-recompile-directory \".\" 0 'force)")))
                 ((require 'elpaca))
                 ((elpaca-generate-autoloads "elpaca" repo)))
            (progn (message "%s" (buffer-string)) (kill-buffer buffer))
          (error "%s" (with-current-buffer buffer (buffer-string))))
      ((error) (warn "%s" err) (delete-directory repo 'recursive))))
  (unless (require 'elpaca-autoloads nil t)
    (require 'elpaca)
    (elpaca-generate-autoloads "elpaca" repo)
    (load "./elpaca-autoloads")))
(add-hook 'after-init-hook #'elpaca-process-queues)
(elpaca `(,@elpaca-order))

;; Uncomment for systems which cannot create symlinks:
;; (elpaca-no-symlink-mode)

;; Install use-package support
(elpaca elpaca-use-package
  ;; Enable use-package :ensure support for Elpaca.
  (elpaca-use-package-mode))

;;When installing a package used in the init file itself,
;;e.g. a package which adds a use-package key word,
;;use the :wait recipe keyword to block until that package is installed/configured.
;;For example:
;;(use-package general :ensure (:wait t) :demand t)

;; Expands to: (elpaca evil (use-package evil :demand t))
;;(use-package evil :ensure t :demand t)

;;; some initital vars and funcs needed before we initialize packages
;; In elisp there is a ignore function, but that evaluates its argument, yielding always nil, whereas comment doesn't evaluate its body. It's useful to temporary disable bits of code.
;; source: https://www.omarpolo.com/dots/emacs.html
(defmacro comment (&rest _body)
  "Ignore BODY, just like `ignore', but this is a macro."
  '())

(when init-file-debug
  (setq use-package-verbose t
        use-package-expand-minimally nil
        use-package-compute-statistics t
        debug-on-error t))

(setq
 mbk/full-name "Milind Kamble"
 mbk/email "milindbkamble@gmail.com")

(load "utils")
(load "customkeybindings")

;; auto-fill-mode and visual-line-mode are builtin. So the third parameter in delight is `t'
;; tried doing this using :delight section in use-package emacs, but it seems emacs package loads
;; and configures before delight is loaded (using :demand t in use-package delight)
;; so settled to using :config section, wherein its content is executed after delight is loaded
;; delight modifies the `minor-mode-alist' with the specification we provide
(use-package delight :ensure (:wait t)
  :config
  (delight '((auto-fill-function " AF" t)
             (visual-line-mode "VF" t))))

;;Turns off elpaca-use-package-mode current declaration
;;Note this will cause evaluate the declaration immediately. It is not deferred.
;;Useful for configuring built-in emacs features.
;; see https://lists.gnu.org/archive/html/help-gnu-emacs/2020-10/msg00305.html (why is emacs a feature)
;; we can use (member 'emacs features) as an alternate to (featurep emacs)
(use-package emacs :ensure nil
;;;; custom section
  :custom
  (ring-bell-function #'ignore)
  (use-package-always-ensure t)
  (use-package-always-defer t)
  (auto-save-visited-interval 5)
  (require-final-newline t)
  ;; based on info emacs::Indent Convenience
  ;; see info elisp::Mode-Specific Indent for how tab-first-complete works
  (tab-always-indent 'complete)
  (tab-first-completion'word-or-paren-or-punct)
  
  ;; view-mode has special key bindings that can be viewed by pressing `h'
  (view-read-only t) ;; making the buffer read-only with ‘C-x C-q’ also enables View mode
  
  ;; don’t compact font caches during GC.
  ;; based on comment in https://github.com/integral-dw/org-bullets where turning on org-bullets slows emacs
  (inhibit-compacting-font-caches t)
  
  ;; outline-minor-mode
  ;;(outline-minor-mode-prefix "\C-c\C-o")
  ;; may not be needed due to outli-mode (outline-minor-mode-cycle t)
  ;; choices are eolp, bolp, not-bolp or custom defined func
  ;; (outline-minor-mode-cycle-filter 'eolp)
  ;; ;; the starting state when outline-mode gets enabled or when outline-apply-default-state is run
  ;; (outline-default-state 2)
  ;; (outline-minor-mode-highlight 'append) ; use outline-1..8 faces for styling headings
  
  ;; ElDoc string in minor-mode-alist is controlled by eldoc-minor-mode-string.
  (eldoc-minor-mode-string nil) ;; default " ElDoc"

  ;; seems a new variable instroduced with emacs-29. Allows remapping standard modes to
  ;; enhanced tree-sitter version
  (major-mode-remap-alist '((python-mode . python-ts-mode)
                            (c-mode . c-ts-mode)
                            (c++-mode . c++-ts-mode)))
  
  ;; treesitter language grammar is provided by corresponding shared libraries
  ;; found this info by web search:  You can either copy the grammar
  ;; library to standard dynamic library locations of your system,eg,
  ;; /usr/local/lib, or leave them in /dist and later tell Emacs
  ;; where to find language definitions by setting ;; ‘treesit-extra-load-path’.
  ;; In guix, there are individual oackages for each language. I am insatlling it in
  ;; $HOME/.guix-extra-profiles directory
  (treesit-extra-load-path (list
			    (expand-file-name  "cli/cli/lib/tree-sitter/"
                                               (getenv "GUIX_EXTRA_PROFILES"))))
  
  ;; ignore case for completions
  (completion-ignore-case t) ; consider case significant in completion.
  (read-file-name-completion-ignore-case t) ; for file-name completion.
  (read-buffer-completion-ignore-case t) ; for buffer name completion.
  
  (indent-tabs-mode nil)         ; use spaces instead of tabs
  (isearch-lazy-count t)         ; Some very small tweaks for isearch
  ;; words seperated by space in isearch input get matched regardless of the intervening chars
  (search-whitespace-regexp ".*?") 
  (isearch-allow-scroll 'unlimited)
  (password-cache-expiry 86400) ; 24hrs before sudo password is prompted again

  ;; `repeat-mode-exit-key' is the key to exit repeat mode. <RET> is recommended in manual and sensible
  (repeat-exit-key "RET")
  
  ;; *** disable native VC backends since we will use magit 
  ;; Feature `vc-hooks' provides hooks for the Emacs VC package. We
  ;; don't use VC, because Magit is superior in pretty much every way.
  (vc-handled-backends nil)

  ;; uses frame parameters (fonts, faces etc) from init file instead from previous session
  (desktop-restore-frames nil)
  (desktop-restore-eager 5)             ; number of buffers to restore immediately. rest are restored lazily
  ;; EPA
  (epa-armor t)
  (epa-file-name-regexp "\\.\\(gpg\\|asc\\)$")
  (epg-pinentry-mode 'ask)	   ; use minibuffer reading passphrase
  (epa-file-encrypt-to (list mbk/email))

  ;; dabbrev is buitlin with emacs
  ;; \(?:regex\) This is called shy group. Group for precedence, but no capture.
  (dabbrev-ignored-buffer-regexps '("\\.\\(?:pdf\\|jpe?g\\|png\\)\\'")) 

;;;; custom-face section
  :custom-face
  (outline-1 ((t (:height 1.25 :weight bold))))
  (outline-2 ((t (:height 1.15 :weight bold))))
  (outline-3 ((t (:height 1.05 :weight bold))))
;;;; config section
  :config
  (global-set-key (kbd "C-x g") 'magit-status)
  (fset 'yes-or-no-p 'y-or-n-p)
  (repeat-mode 1)    ; turn on repeat mode. it is a global minor mode
  ;; instead of ellipsis (...) we display a glyph when lines are hidden. see info::Display Tables
  ;; from https://emacs.stackexchange.com/questions/10981/changing-the-appearance-of-org-mode-hidden-contents-ellipsis
  (set-display-table-slot standard-display-table 
                          'selective-display (string-to-vector "⤵"))
  ;; As per Basti's blog https://bastibe.de/2014-05-07-speeding-up-org-publishing.html
  ;; removing vc-refresh-state from find-file-hook speeds up org-static-blog
  (remove-hook 'find-file-hook 'vc-refresh-state)
  (remove-hook 'kill-buffer-hook #'vc-kill-buffer-hook)

  ;; Use Victor Mono as fixed pitch font
  (set-face-attribute 'default nil :family "Victor Mono" :height 120)
  ;; As per https://protesilaos.com/codelog/2020-09-05-emacs-note-mixed-font-heights/
  ;; we need to specify height of other fonts relative to default for text-scaling to work
  ;; implicit height does not work, we need to explicitly specify the :height 1.0
  (set-face-attribute 'fixed-pitch nil :family "Victor Mono" :height 1.0)
  (set-face-attribute 'variable-pitch nil :family "Source Sans 3" :height 1.0) ;; for proportional or dynamic font width
  (variable-pitch-mode -1) ;; disable varibale-pitch by default
  (defalias 'proportional-font-mode 'variable-pitch-mode
    "porportional-font is easier to remember than variable-pitch") ;; my own alias since I tend to forget `variable-pitch-mode'

  (defalias 'toggle-wrap-lines 'toggle-truncate-lines
    "wrap-lines is easier to remember than truncate-lines") ;; an easier to remember alias for toggling line wrapping
;;;; hook section
  ;; :hook (
         ;; remember do not specify the hook variable, but only the mode for the
         ;; first argument. use-package auto adds the '-hook' suffix
         
         ;; turn on outline-minor mode in prog and text modes ad major modes derived from them
         ;;((prog-mode text-mode) . outline-minor-mode)
         ;; bind "C-c C-c" to outline prefix map
         ;; (outline-minor-mode . (lambda ()
         ;;                         (local-set-key "\C-c\C-o" outline-mode-prefix-map)))
         ;; outline-regexp defines which lines are to be treated as outline headings
         ;; outline-regexo is defined in lisp/outline.el to be wither a line that beigns with
         ;; 3 semicolons or starts with open-paren or lisp-mode-regexp
         ;; I override that to be only 3 semicolons. I am not interested in code view collapsing
         ;; without explicitly intentioned headings. It also simplifies prety-bullets
         ;; (lisp-data-mode .
         ;;                 (lambda ()
         ;;                   (setq outline-regexp  ";;;;* [^ \t\n]")))
  ;;)
;;;; bind section
  :bind (:map goto-map ("u" . unlock-keepass)
         :map mode-specific-map ("r" . consult-minor-mode-menu))
)


;;; initialzie packages
;;;; `no-littering'
;; for elpaca we need  to use wait so that no-littering is loaded before proceeding.
;; related to eplaca-process-queues
;; See https://www.reddit.com/r/emacs/comments/1era10n/load_nolittering_in_earlyinit/
;; to deal with eln-cache, we added code to early-init
(use-package no-littering
  :ensure (:wait t)
  :config
  (require 'recentf)
  (add-to-list 'recentf-exclude no-littering-var-directory)
  (add-to-list 'recentf-exclude no-littering-etc-directory)
  (auto-save-visited-mode 1) ; automatic saving of file-visiting buffers to their files
  (savehist-mode 1) ; enable saving of minibuffer history to `savehist-file'. emacs builtin
  (desktop-save-mode 1)  ; save desktop when exiting Emacs
  (recentf-mode 1)
  :custom ((auto-save-file-name-transforms     ; transform names of saved files
            `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
 (backup-directory-alist ; store all backup and autosave files in "backups"
   `((".*" . ,(no-littering-expand-var-file-name "backups/"))))
   (custom-file (no-littering-expand-etc-file-name "custom.el"))
   (recentf-max-saved-items 3001)) ; since we modify the recentf-save-file, we wait until no-littering has made those changes then enable recentf-mode which then loads the recentf-save-file
  )

;;;; `all-the-icons'
(use-package all-the-icons
  :if (display-graphic-p)
  :demand t
  :config
  (set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'prepend)
  (set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'prepend)
)


;;;; `undo-tree'
(use-package undo-tree
  :delight                              ; hide mode-line indicator
  :bind (("C-x u" . undo-tree-visualize))
  :config
  (global-undo-tree-mode)
  (no-littering-theme-backups))
;;;; `smartparens' handles paired punctuations
;; [[https://www.wisdomandwonder.com/article/9897/use-package-smartparens-config-ensure-smartparens][here]]
(use-package smartparens
  ;; :delight " SP"
  :bind (:map smartparens-mode-map
	      ;; see variable `sp-smartparens-bingdings' in package source `smartparens.el'
					; use default ("C-M-a" . sp-beginning-of-sexp) 
					; use default ("C-M-e" . sp-end-of-sexp)
					; use default ("C-(" . sp-forward-barf-sexp)
					; use default ("C-)" . sp-forward-slurp-sexp)
					; use default ("C-{" . sp-backward-barf-sexp)
					; use default ("C-}" . sp-backward-slurp-sexp)
	      
              ("C-k" . sp-kill-hybrid-sexp)
	      ("C-," . sp-rewrap-sexp)
	      :map emacs-lisp-mode-map (";" . sp-comment)
	      :map lisp-mode-map (";" . sp-comment)
	      )
  :config
  (setq sp-show-pair-from-inside nil)
  (require 'smartparens-config)
  (smartparens-global-mode)
  (sp-with-modes 'org-mode
    (sp-local-pair "=" "=" :wrap "C-="))
  (bind-key [remap c-electric-backspace] #'sp-backward-delete-char
            smartparens-strict-mode-map)
  ;; (unbind-key "M-<delete>" smartparens-mode-map) ; M-<delete> will perform sp-unwrap-sexp (forward looking)
  (unbind-key "M-<backspace>" smartparens-mode-map) ; I prefer M-<backspace> to perform backward-kill-word

  :hook ((prog-mode . turn-on-smartparens-strict-mode)
         (web-mode . op/sp-web-mode)
         (LaTeX-mode . turn-on-smartparens-strict-mode))
  :custom ((sp-highlight-pair-overlay nil)
	   (sp-base-key-bindings 'sp)))
(use-package highlight-parentheses
  :delight
  :config (global-highlight-parentheses-mode))

;;;; `ace-window' and `ace-link'
;;;;; notes
;; the default value of aw-dispatch-alist is
;; ("?b" aw-split-window-horz "Split Horz Window")          ; my preference ?-
;; ("?c" aw-copy-window "Copy Window")
;; ("?e" aw-execute-command-other-window "Execute Command Other Window")  ; my preference ?!
;; ("?F" aw-split-window-fair "Split Fair Window")
;; ("?j" aw-switch-buffer-in-window "Select Buffer")
;; ("?m" aw-swap-window "Swap Windows")
;; ("?M" aw-move-window "Move Window")
;; ("?n" aw-flip-window)
;; ("?o" delete-other-windows "Delete Other Windows")  ; my preference ?d
;; ("?T" aw-transpose-frame "Transpose Frame")
;; ("?u" aw-switch-buffer-other-window "Switch Buffer Other Window")   ; my preference ?s
;; ("?v" aw-split-window-vert "Split Vert Window")       ; my preference ?|
;; ("?x" aw-delete-window "Delete Window")
;; ("??" aw-show-dispatch-help)
;; This dispatch is called when we hit M-o
;; We want to use a,o,e,u,h for window hints. So we should use d,!,s for conflicts with o,e,u
;;;;; configuration
(use-package ace-window
  :custom
  (aw-keys '(?a ?o ?e ?u ?h))
  ;; aw-swap-invert moves point to target window, thus keeping point in the orig buffer
  (aw-swap-invert t)
  ;; aw will perform read-char even when there is single window. allows us to split windows, switch buffers etc.
  (aw-dispatch-always t) 

  :bind
  (("M-o" . #'ace-window))              ; bind in global-map
  :init
  ;; need to do this in `init' because we will defvar it before package is loaded
  ;; and since it is defvar also in `ace-window.el', that definition will turn into a No-op
  ;; because defvar does nothing if the variable is already defined
  (defvar aw-dispatch-alist
    '((?b aw-switch-buffer-in-window "Select Buffer")
      (?c aw-copy-window "Copy Window")
      (?d delete-other-windows "Delete Other Windows")
      (?f aw-split-window-fair "Split Fair Window")
      (?m aw-move-window "Move Window")
      (?n aw-flip-window)
      (?r aw-switch-buffer-other-window "Switch Buffer Other Window")
      (?t aw-transpose-frame "Transpose Frame")
      (?w aw-swap-window "Swap Windows")
      (?x aw-delete-window "Delete Window")
      (?- aw-split-window-horz "Split Horz Window")
      (?| aw-split-window-vert "Split Vert Window")
      (?! aw-execute-command-other-window "Execute Command Other Window")
      (?? aw-show-dispatch-help))
    "List of actions for `aw-dispatch-default'.
Each action is a list of either:
  (char function description) where function takes a single window argument
or
  (char function) where function takes no argument and the description is omitted."))
;;;;; `ace-link' enables hinting of embedded links
;;     in Info, Help, woman, eww, compilation and custom-mode-map
;; TBD: (define-key org-mode-map (kbd "somekey") 'ace-link-org)
(use-package ace-link
  :config (ace-link-setup-default))
;;;; `expand-region' for selecting text objects
(use-package expand-region
  :bind (("C-=" . er/expand-region)))
;;;; `which-key'
;; setting prefix-help-command to my  preferred version based on reading at
;; https://with-emacs.com/posts/ui-hacks/prefix-command-completion/
(use-package which-key
  :after embark
  ;; :demand t
  :delight
  :init
  (setq prefix-help-command #'embark-prefix-help-command)
  (which-key-mode 1))                   ; turn on which-key needs to be in init not in config
;;;; `s' the string manipulation library
(use-package s)
;;;; `eshell'
;; Eshell key bindings are stored in eshell-mode-map. to use the key binding mechanism
;; of use-package, eshell-mode-map must actually be defined. So we do a require in :init
;; see https://www.n16f.net/blog/eshell-key-bindings-and-completion/
(use-package eshell
  :ensure nil
  :init
  (require 'esh-mode) ; eshell-mode-map
  :custom
  (eshell-banner-message "")
  (eshell-scroll-to-bottom-on-input t)
  (eshell-error-if-no-glob t)
  (eshell-hist-ignoredups t)
  (eshell-save-history-on-exit t)
  (eshell-prefer-lisp-functions nil)
  ;; addding history-references to input-functions enable the use of !foo:n
  ;; to insert the nth arg of last command beg with foo
  ;; or !?foo:n for last command containing foo
  (add-to-list 'eshell-expand-input-functions 'eshell-expand-history-references)
  (add-to-list 'eshell-modules-list 'eshell-rebind)
  (add-to-list 'eshell-modules-list 'eshell-tramp)
  ;; (eshell-destroy-buffer-when-process-dies t)
  ;; (eshell-highlight-prompt nil)

  :config
  (setenv "PAGER" "cat")
  ;; (require 'eshell-z)
  (require 'em-tramp)
  :bind
  (
   ;; :map global-map (("C-x s" . g-eshell))
   :map eshell-mode-map
   (("M-." . mbk/eshell-recall-args)
    )))

;;;; `eshell-z'
(use-package eshell-z
  :after eshell
  :custom
  (eshell-z-freq-dir-hash-table-file-name
   (concat eshell-directory-name "z-dir-table"))
  (eshell-history-size 10000))

;;;; `poet' theme
(defadvice load-theme (after mbk/run-after-load-theme-hook activate)
  "Run `mbk/after-load-theme-hook'."
  ;; debug (message (format "mbk: calling run-hooks. mbk/after-load-theme-hook=%s" mbk/after-load-theme-hook))
  (run-hooks 'mbk/after-load-theme-hook))
(defun mbk/customize-poet ()
  "Customize poet theme"
  (if (member 'poet custom-enabled-themes) ; use `progn' to add debug msg for `then' clause
      (custom-theme-set-faces 'user ; need to use `user'. `poet is overridden by `user''
			      '(font-lock-comment-face ((t (:slant italic)))))))
(use-package poet-theme
  :init
  (add-hook 'mbk/after-load-theme-hook #'mbk/customize-poet)  ; need to add-hook before loading theme
  (load-theme 'poet t)    ; see `custom-available-themes' for optional themes
  )
;;;; `outli-mode'
(use-package outli :ensure (:host github :repo "jdtsmith/outli")
  :bind (:map outli-mode-map ; convenience key to get back to containing heading
	      ("C-c C-p" . (lambda () (interactive) (outline-back-to-heading))))
  :hook (((prog-mode text-mode) . outli-mode)))
;;;; `magit' and dependencies
;; we use-package all dependencies explicitly because magit gives error that
;; transient bundled with emacs distrib is lower than required
;; `compat' is already loaded by no-littering
(use-package transient)
(use-package dash)
(use-package with-editor)
(use-package magit
  :after transient
  :config
  (setopt magit-diff-refine-hunk t
          transient-default-level 5))

;;;; `vertico', `marginalia', `consult', `embark', `orderless'
;;;; `vertico'
(use-package vertico
;;;;; bind
  :bind
  (:map vertico-map
        ("C-j" . vertico-next)
        ("C-k" . vertico-previous)
	("M-h" . vertico-directory-up)) ;; `vertico-directory-up' shortens the target by directory separator
  ;; :blackout (foo-mode . " Foo") ; results in (blackout 'foo-mode " Foo")
  :demand t
;;;;; custom
  :custom
  (completion-in-region-function #'consult-completion-in-region)
  (vertico-scroll-margin 0)             ; Scroll margin
  ;; (setq vertico-count 10)        ; Candidates
  (vertico-cycle t)             ; Enable cycling when at top or bottom
;;;;; config
  :config
  ;; this call was placed in init instead of config section as per vertico documentation. but it seems it works in config section too and I feel it is more appropriate
  (vertico-mode 1))

;;;;; additonal tweak recommended by vertico author
;; Add prompt indicator to `completing-read-multiple'.
;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
(defun crm-indicator (args)
  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))
(advice-add #'completing-read-multiple :filter-args #'crm-indicator)

;; Do not allow the cursor to move into prompt area of minibuffer
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)
;; end recommendation by vertico author
;;;; `consult'
(use-package consult
  :bind
  (
   ;; these are all bound in global-map
   ;; since ctl-x-map is bound to C-x in global-map there is a key sequence in the global-map to each of the following commands listed after the remap. Hence our remapping to consult-flavored equivalents works
   ([remap switch-to-buffer] . consult-buffer)
   ([remap switch-to-buffer-other-window] . consult-buffer-other-window)
   ([remap yank-pop] . consult-yank-pop)
   ([remap bookmark-jump] . consult-bookmark)
   ([remap repeat-complex-command] . consult-complex-command)
   ([remap project-switch-to-buffer] . consult-project-buffer)
   ([remap goto-line] . consult-goto-line)
   ([remap next-matching-history-element] . consult-history)
   ([remap prev-matching-history-element] . consult-history)
   ([remap isearch-edit-string] . consult-isearch-history)
   ("C-c h" . consult-history) ; access current buffer history. useful for eshell, comint. useable even in mini-buffer
   ("C-c i" . consult-info)
   ("C-c M-x" . consult-mode-command) ; like M-x but filter command related to mojor mode
   ("C-c k" . consult-kmacro) ; exec a macro from macro-ring
   ("M-#" . consult-register-load)
   ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
   ("C-M-#" . consult-register)
   :map project-prefix-map
   ("i" . consult-project-imenu)
   :map goto-map
   ;; goto-map is defined in bindings.el.gz and by default only the following are setup
   ;; c . goto-char
   ;; g . goto-line
   ;; n . next-error
   ;; p . previous-error
   ;; TAB . move-to-column
   ("e" . consult-compile-error)
   ("i" . consult-imenu)
   ("I" . consult-imenu-multi)
   ("k" . consult-global-mark)
   ("l" . consult-line)
   ("m" . consult-mark)
   ("o" . consult-outline)
   :map search-map
   ("G" . consult-git-grep)
   ("L" . consult-line-multi)
   ("b" . flush-blank-lines)
   ("d" . consult-find)
   ("e" . consult-isearch-history) ; Isearch integration
   ("g" . consult-grep)
   ("k" . consult-keep-lines)
   ("l" . consult-line)
   ("r" . consult-ripgrep)
   ("u" . consult-focus-lines)
   :map isearch-mode-map
   ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
   ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
   ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
   ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
   ;; Minibuffer history
   :map minibuffer-local-map
   ("M-s" . consult-history)                 ;; orig. next-matching-history-element
   ("M-r" . consult-history)                ;; orig. previous-matching-history-element

   :map help-map
   ("M" . consult-man))
  :custom
  (register-preview-delay 0)
  (register-preview-function #'consult-register-format)
  (consult-narrow-key "<")
  :config
  ;; the following customization to prevent auto-preview and instead use manual preview came from
  ;; README.org file of consult (https://github.com/minad/consult) -- loading recent files or bookmarks may result in expensive operations, so do manual preview
  (consult-customize
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   :preview-key "M-.")

  ;; closely based on `consult--source-recent-file' but using `recently'
  ;; (defvar consult--source-recently-file
  ;; `(:name     "File"
  ;;   :narrow   ?z
  ;;   :category file
  ;;   :face     consult-file
  ;;   :history  file-name-history
  ;;   :state    ,#'consult--file-state
  ;;   :new      ,#'consult--file-action
  ;;   :enabled  ,(lambda () recently-mode)
  ;;   :items
  ;;   ,(lambda ()
  ;;      (let ((ht (consult--buffer-file-hash))
  ;;            items)
  ;;        (dolist (file (bound-and-true-p recentf-list) (nreverse items))
  ;;          ;; Emacs 29 abbreviates file paths by default, see
  ;;          ;; `recentf-filename-handlers'.  I recommend to set
  ;;          ;; `recentf-filename-handlers' to nil to avoid any slow down.
  ;;          (unless (eq (aref file 0) ?/)
  ;;            (let (file-name-handler-alist) ;; No Tramp slowdown please.
  ;;              (setq file (expand-file-name file))))
  ;;          (unless (gethash file ht)
  ;;            (push (consult--fast-abbreviate-file-name file) items))))))
  ;; "Recent file candidate source for `consult-buffer'.")
  ;; (delq 'consult--source-recent-file consult-buffer-sources)
  ;; (add-to-list 'consult-buffer-sources 'consult--source-recently-file 'append)
  )


;; (defun consult-beancount-account-new (&optional arg)
;;   (interactive "p")
;;   (let ((acct (consult--read #'mbk/beancount-account-completion-table :prompt "Acct: " :predicate nil :require-match t))))
;;   (insert acct))
(defun consult-beancount-account (&optional arg)
  "Insert a beancount account. Universal or > 1 prefix will refresh the candidate list"
  ;; "p" gives us numeric form of prefix
  ;;  arg = 1 if prefix eq nil
  ;;       = -1 if prefix is '-' (M-- or C-u -)
  ;;       = -n if prefix is -n (M-- n or C-u -n) 
  ;;       = +n if prefix is n (M-n or C-u n)
  ;;       = 4, 16, 64 etc if prefix is repeated C-u
  ;; so if arg > 1 or beancount-accounts is null, we refresh beancount-accounts
  (interactive "p")
  (if (> arg 0) (progn
		  (message "buufer=%s beancount-accounts.len=%d"
			   (current-buffer)
			   (length beancount-accounts))
		  (setq beancount-accounts nil)))
  (let ((acct (consult--read
	       (consult--completion-table-in-buffer #'beancount-account-completion-table)
	       :prompt "Acct: " :predicate nil :require-match t)))
    (insert acct)))
;;;; `embark'
;; `embark' package for mini-buffer actions and right-click contextual menu functionality
;; Embark provides custom actions on the minibuffer (technically everywhere)
(use-package embark
  :demand t
  :bind
  (("C-." . embark-act)         ;; pick some comfortable binding
   ("C-;" . embark-dwim)        ;; good alternative: M-.
   ("C-h B" . embark-bindings)) ;; alternative for `describe-bindings'

  :config
  ;; Optionally replace the key help with a completing-read interface
  (setq prefix-help-command #'embark-prefix-help-command)
  
  ;; Hide the mode line of the Embark live/completions buffers
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))
(use-package embark-consult
  :after (embark consult))
;;;; `oerderless', `affe'
;; `orderless': - a completion style using space-separated patters in any order
(use-package orderless
  :custom
  ;; basic completion style is specified as fallback in addition to orderless in order to ensure that completion commands which rely on dynamic completion tables, e.g., completion-table-dynamic or completion-table-in-turn, work correctly
  (completion-styles '(orderless basic)
                     "orderless is preferred completion-style with basi as fallback")
  ;; (completion-category-defaults nil "") ; no longer present?
  ;; enable partial-completion for file path expansion. partial-completion is important for file wildcard support. Multiple files can be opened at once with find-file if you enter a wildcard. You may also give the initials completion style a try.
  (completion-category-overrides '((file (styles . (partial-completion)))) "enable partial-completion for file path expansion"))
;; affe: asynchronous fuzzy finder for emacs
(use-package affe
  :after orderless
  :custom ((affe-regexp-function #'orderless-pattern-compiler)
           (affe-highlight-function #'orderless-highlight-matches)))
;;;; `marginalia'
;; `marginalia': enable richer annotations including richer annotation of completion candidates
(use-package marginalia
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  :custom
  (marginalia-annotators
   '(marginalia-annotators-heavy marginalia-annotators-light nil))
  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode))

;;;; `org', `org-bullets'
(use-package org-bullets
  :hook
  (org-mode . org-bullets-mode))
(use-package org :ensure nil
  :custom
  (org-use-speed-commands t)              ; single keystroke bindings when cursor is on BOL of heading
  (org-startup-indented t)
  (org-adapt-indentation nil)
  (org-edit-src-content-indentation 0)
  (org-outline-path-complete-in-steps nil)
  (org-ellipsis "⤵")   ; alternate: " ▾"
  (org-hide-emphasis-markers t)
  (org-log-into-drawer t)
)
;;;; `yaml' parser in Elisp
;; will switch to `yamo-pro' later
(use-package yaml)
;;;; `yasnippet', `yasnippet-snippets
;; we do :deman t because there is no other way to get it auto loaded
(use-package yasnippet :demand t
  :config
  (yas-global-mode 1))'
(use-package yasnippet-snippets)
;;;; `keycast'
(use-package keycast)
;;;; `csv-mode'
(use-package csv-mode)
;;;; `sicp'
(comment use-package sicp)
;;;; `eglot'
;; see https://clangd.llvm.org/installation.html
(use-package eglot :ensure nil
  :hook ((c-mode c++-mode c-ts-mode c++-ts-mode
                 python-base-mode) . eglot-ensure))
;;;; `beancount'
(use-package beancount :ensure (:host gitlab :repo "milind.b.kamble/beancount-mode")
;;;;; :custom  
  :custom
  (beancount-use-ido nil)
  :mode (("\\.\\(beancount\\|bct\\)\\'" . beancount-mode)) ; the last quote signifies extension withiut asc suffix
;;;;; bind
  :bind
  (:map beancount-mode-map
	("C-c C-n" . #'beancount-goto-next-transaction)
	("C-c C-p" . #'beancount-goto-previous-transaction)
	;; ("C-c C-a" . #'mbk/beancount-goto-transaction-begin)
	;; ("C-c C-e" . #'beancount-goto-transaction-end)
        ;; ("M-["     . #'consult-beancount-account)
	("C-c '" . #'consult-beancount-account))
  (:repeat-map my/beancount-mode-map-repeat-map
	       ;; ("a" . beancount-goto-transaction-begin)
	       ;; ("e" . beancount-goto-transaction-end)
	       ("n" . beancount-goto-next-transaction)
	       ("p" . beancount-goto-previous-transaction))
;;;;; :config
  :config
  (defun bc-simple-dt (dt)
    "Read a string of form [[YEAR] MM] DD and convert it to beancount compliant date. single digits are appropriately padded. Default value for YEAR is file-variable `bc-year' or 2000 and for MM is 01"
    (interactive "M")
    ;; (message "dt=%s" dt)
    (defun mth-dt-pad0 (mth-or-dt)
      (substring (concat "00" mth-or-dt) -2 nil))
    (defun yr-pad (yr)
      (concat "20" (mth-dt-pad0 yr)))
    (pcase (split-string dt)
      (`(,y ,m ,d) (format "%s-%s-%s" (yr-pad y) (mth-dt-pad0 m) (mth-dt-pad0 d)))
      (`(,m ,d) (format "%s-%s-%s" (if (boundp 'bc-year) bc-year "2000") (mth-dt-pad0 m) (mth-dt-pad0 d)))
      (`(,d) (format "%s-%s-%s" (if (boundp 'bc-year) bc-year "2000") (mth-dt-pad0 d)))
      (_ (error "unsupported input form"))))
  )
;;;; `free-keys
(use-package free-keys)
;;;; to be considered
;; `ace-link'
;; `recentf-ext'
;; `sly' IDE for common lisp
;; `python-mode'
;; `markdown-mode'
;; `highlight-parentheses'
;; `corfu'
;; `yaml-pro-mode' a minor mode that probably augments the builtin major mode `yaml-ts-mode'
;; `forge'  magit interface to git forges like github, gitlab etc
;; `org-roam'
;; `cheatsheet'

;;;; extra code
(comment progn
 (use-package consult  ;; source: https://www.omarpolo.com/dots/emacs.html
  :bind (("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c b" . consult-bookmark)
         ("C-c k" . consult-kmacro)
         ("C-x M-:" . consult-complex-command)
         ("C-x b" . consult-buffer)
         ("C-x 4 b" . consult-buffer-other-window)
         ("C-x 5 b" . consult-buffer-other-frame)
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)
         ("C-M-#" . consult-register)
         ("M-g e" . consult-compile-error)
         ("M-g g" . consult-goto-line)
         ("M-g M-g" . consult-goto-line)
         ("M-g o" . consult-outline)
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-project-imenu)
         ("M-s f" . op/consult-find)
         ("M-s g" . consult-grep)
         ("M-s l" . consult-line)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ("M-s e" . consult-isearch))
  :custom ((register-preview-delay 0)
           (register-preview-function #'consult-register-format)
           ;; use consult to select xref locations with preview
           (xref-show-xrefs-function #'consult-xref)
           (xref-show-definitions-function #'consult-xref)
           (consult-narrow-key "<")
           (consult-project-root #'project-roots)
           (consult-find-args "find .")
           (consult-grep-args "grep --null --follow --line-buffered --ignore-case -RIn"))
  :init
  (advice-add #'register-preview :override #'consult-register-window)

  :config
  ;; make narrowing help available in the minibuffer.
  (define-key consult-narrow-map (vconcat consult-narrow-key "?")
              #'consult-narrow-help)

  ;; a find-builder that works with OpenBSD' find
  (defun op/consult--find-builder (input)
  "Build command line given INPUT."
  (pcase-let* ((cmd (split-string-and-unquote consult-find-args))
               (type (consult--find-regexp-type (car cmd)))
               (`(,arg . ,opts) (consult--command-split input))
               (`(,re . ,hl) (funcall consult--regexp-compiler arg type)))
    (when re
      (list :command
            (append cmd
                    (cdr (mapcan
                          (lambda (x)
                            `("-and" "-iname"
                              ,(format "*%s*" x)))
                          re))
                    opts)
            :highlight hl))))

  (defun op/consult-find (&optional dir)
    (interactive "P")
    (let* ((prompt-dir (consult--directory-prompt "Find" dir))
           (default-directory (cdr prompt-dir)))
      (find-file (consult--find (car prompt-dir) #'op/consult--find-builder "")))))

;; bindings used by omarpolo
(use-package embark
  :bind (("C-." . embark-act)
         :map minibuffer-local-completion-map
         ("M-t" . embark-act)
         ("M-h" . embark-become)
         :map minibuffer-local-map
         ("M-t" . embark-act)
         ("M-h" . embark-become)))
)

;; Local Variables:
;; no-byte-compile: t
;; no-native-compile: t
;; no-update-autoloads: t
;; End:
