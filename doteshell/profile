#  -*- mode: lisp -*-
#
#  from github:howardabrams:dot-files:emacs-eshell.org
#  gst command is just an alias to magit-status, but using the alias
#  doesn’t pull in the current working directory, so make it a
#  function
(eshell/echo "sourcing eshell-profile")

(defun eshell/gst (&rest args)
    (magit-status (pop args) nil)
    (eshell/echo))   # The echo command suppresses output

(defun eshell-command-result-filter (command rex)
       "return list of REX matching lines from output of COMMAND"
       (seq-filter
        (lambda (x) (s-matches? rex x))
          (s-lines (eshell-command-result command))))

;; battery status is available via builtin `battery' command

(defun eshell/piavpn ()
  (let*
      ((secret-string
	(epg-decrypt-file (epg-make-context epa-protocol)
			  "/home/mbkamble/.gnupg/keepass/keepass_safebox.gpg" nil))
       (pia-htbl (gethash "pia" (json-parse-string secret-string)))
       (login (gethash "login" pia-htbl))
       (passwd (gethash "passwd" pia-htbl))
       (region (completing-read "regions: " '("in" "us_south_west" "us_houston" "") nil nil nil nil "us_south_west")))
    (format "%s ## %s ## %s" login passwd region)))

(defun eshell/kbsetup (key &optional reset)
  "change keyboard layout using setxkbmap under Xwindows"
  (let*
      ((match-patt (cond
                    ((s-equals? key "aaaa") "AT Translated.+keyboard")
                    ((s-equals? key "dybo") "Sofle\\s-+id.+keyboard")
                    ;; \s- in elisp regex for whitespace. using \\\s is a pitfall bcos then match-patt becomes '\ +' as \s is interpolated into a space
                    "dont care"
                    ))
       (kbid-from-xinput (car (eshell-command-result-filter "xinput list" match-patt)))
       (kbid (if kbid-from-xinput (caddr (s-split "id=\\|\t"
                                                  kbid-from-xinput))))
       (setxkbmap-options (format "-device %s -layout us %s" kbid
                                  (if reset "-option" "-variant dvorak -option ctrl:nocaps"))))
    (if kbid (eshell-plain-command "setxkbmap" (s-split " " setxkbmap-options)))
    (format "key=%s patt=%s kbid-from-xinput=%s kbid=%s xkbmap-options=%s" key match-patt kbid-from-xinput kbid setxkbmap-options)))
